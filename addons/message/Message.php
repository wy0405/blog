<?php
/**
 * Created by PhpStorm.
 * User: 29139
 * Date: 2019/10/17
 * Time: 18:37
 */

namespace addons\message;
use think\Db;
use think\Addons;

class Message extends Addons	// 需继承think\addons\Addons类
{
    // 该插件的基础信息
    public $info = [
        'name' => 'message',	// 插件标识
        'title' => '在线留言',	// 插件名称
        'description' => '在线留言',	// 插件简介
        'status' => 0,	// 状态
        'author' => 'wy',
        'version' => '0.1',
        'menu'=>[
            'name'=>'查看留言',
            'url'=>'',
            'type'=>0, //0:个人博客管理后台菜单，1：博客系统后台管理菜单
        ]
    ];
    //控制器初始化
    public function _initialize()
    {
        //初始化menu里面的url
        $this->info['menu']['url']=addon_url('message://Index/lst');

    }

    /**
     * 插件安装方法
     * @return bool
     */
    public function install()
    {
        //拿到基本信息 写入配置文件

        //组织配置文件内容
        $content = $this->info;
        //创建留言数据表
        //获取sql文件
        $sql_content = file_get_contents(ADDON_PATH."message/install.sql");
        //array_filter对空值进行过滤 explode根据分号拆分
        $arr_sql=array_filter(explode(";",$sql_content));
//        halt($arr_sql);die;
        $sql_status=true;
        foreach($arr_sql as $v){
            //sql语句返回的是影响行数 要强制转为布尔型
            $a = Db::execute($v);
            //$a 不等于false就为true
            if($a !==false){
                $a = true;
            }
            // &&同时为true时才返回true
            $sql_status = $a  && $sql_status;
        }
        if($sql_status===false){
            return false;
        }

        $result = create_config($content);
        if($result){
            return true;
        }else{
            return false;
        }
        //生成配置文件
        $result = create_config($content);
        if($result==false){
            return $result;
        }
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
        //删除数据表 IF EXISTS 数据表存在则进行删除
        $sql = "DROP TABLE IF EXISTS `td_message`";
        $sql_status = Db::execute($sql);
        //SQL语句删除返回的是影响1条行数 要用恒等于
        if($sql_status === false){
            return false;
        }
        //删除config文件
        //获取当前模块名
        $name = $this->getName();
        //组织config路径
        $file=ADDON_PATH.$name.DS.'config.php';
        //判断该路径下的文件 是否是文件类型
        if(!is_file($file)){
            return false;
        }

        //执行删除文件
        $result=@unlink($file);
        if(!$result){
            return false;
        }
        return true;
    }

    /**
     * 实现的testhook钩子方法
     * @return mixed
     */
    public function messagehook($param)
    {
//        dump($param);
//        dump($param);die;
        $this->assign('cid',$param["cid"]);
        //加载模版
        return $this->fetch('message');
    }
}