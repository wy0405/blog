<?php
/**
 * Created by PhpStorm.
 * User: 29139
 * Date: 2019/10/18
 * Time: 15:05
 */

namespace addons\message\model;


use think\Model;

class Message extends Model
{
    // 设置当前模型对应的完整数据表名称
    protected $table = 'td_message';
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp =true;      //默认创建时间字段为 create_time ， 更新时间字段为 update_time
    public static function store($data){
        //判断是不是数组
        if(!is_array($data)){
            return false;
        }
//        dump($data);
        //获取博文发布作者（uid） 根据分类cid去找用户uid
        $touid = db("blogcate")->where('cid',$data['cid'])->value('uid');//value查询某个字段的值可以用
//        dump($touid);die;
        $data["touid"]=$touid;
        unset($data['cid']);
//        dump($data);die;
        //使用模型静态方法新增数据
        $result = self::create($data);
        if($result){
            return true;
        }
        return false;
    }

}