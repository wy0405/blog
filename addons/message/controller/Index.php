<?php
/**
 * Created by PhpStorm.
 * User: 29139
 * Date: 2019/10/18
 * Time: 3:19
 */

namespace addons\message\controller;


use app\common\controller\Base;
use think\addons\Controller;
use addons\message\model\Message;
class Index extends Controller
{
    //留言列表
    public function lst(){
        $this->checklogin();
        $list=(new Message())->paginate('5');
        // 把分页数据赋值给模板变量list
        $this->assign('list', $list);
        //把获取的留言菜单分配到模板
        $addonmenu=(new Base())->getaddonsmenu();
        $this->assign('addonmenu',$addonmenu);
        // 渲染模板输出
        return $this->fetch();
    }
    /*
     * 在线留言数据处理
     * */
    public function save(){
        //接收表单提交数据
        if(!request()->isPost()){
            return '参数错误';
        }
        $data = input('post.');
//        引入model类处理数据（新增数据）
        $result = Message::store($data);
        if($result){
            $this->success("留言成功");
        }else{
            $this->error("留言失败");
        }

    }
    //删除留言
    public function del(){
        $this->checklogin();
        $id = input('post.id');
        $result = Message::destroy($id);
        if($result>0){
            return json(['code'=>1,'msg'=>'删除成功']);
        }
        return json(['code'=>0,'msg'=>'删除失败']);

    }
    //判断登录状态
    public function checklogin(){
        if(!Base::isLogin()){
            $this->success('无权操作','blog/admin/index');
        }
    }

}