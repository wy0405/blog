<?php
/**
 * Created by PhpStorm.
 * User: 29139
 * Date: 2019/10/16
 * Time: 16:17
 */

namespace addons\youqing;

use think\Addons;
/**
 * 插件测试
 * @author byron sampson
 */
class Youqing extends Addons	// 需继承think\addons\Addons类
{
    // 该插件的基础信息
    public $info = [
        'name' => 'youqing',	// 插件标识
        'title' => '友情链接插件',	// 插件名称
        'description' => '友情链接',	// 插件简介
        'status' => 0,	// 状态
        'author' => 'byron sampson',
        'version' => '0.1'
    ];

    /**
     * 插件安装方法
     * @return bool
     */
    public function install()
    {
        //拿到基本信息 写入配置文件

        //组织配置文件内容
        $content = $this->info;
        $content['menu'] = [];
        //生成配置文件
        $result = create_config($content);
        if($result==false){
            return $result;
        }
    }

    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
        //ADDON_PATH
        //获取当前模块名
        $name = $this->getName();
        //组织config路径
        $file=ADDON_PATH.$name.DS.'config.php';
        //判断该路径下的文件 是否是文件类型
        if(!is_file($file)){
            return false;
        }
        //执行删除文件
        $result=@unlink($file);
        if(!$result){
            return false;
        }
        return true;

        return true;
    }

    /**
     * 实现的testhook钩子方法
     * @return mixed
     */
    public function youqinglianjie($param)
    {
        // 调用钩子时候的参数信息
//        print_r($param);
//        // 当前插件的配置信息，配置信息存在当前目录的config.php文件中，见下方
//        print_r($this->getConfig());
        // 可以返回模板，模板文件默认读取的为插件目录中的文件。模板名不能为空！
        return $this->fetch('info');
    }

}