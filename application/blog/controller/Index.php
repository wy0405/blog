<?php 
namespace app\blog\controller;
use app\common\controller\Base;
use app\common\model\User;
class Index extends Base{
	public function index(){
//        $username=decrypt(Cookie('name'),SALT);
        //查询
        $result = User::where('account',decrypt(cookie("name"),SALT))
            ->field('logo,blogtitle,blogname,host,content,info')
            ->find();
        //toArray转换为数组
//		dump($result);die();
        $result=$result->toArray();
		//更新数据
        if(request()->isPost()){
        	$data = input('post.');
//        	获取文件对象   file(表单里的name值)
            // 获取表单上传文件 例如上传了001.jpg
            $file = request()->file('logo');
            $fileinfo = Base::upload($file,$result['logo']);
            if($fileinfo['code']==1){
            	$data['logo']=$fileinfo['path'];
			}
//        	验证器
			$validate = validate('User');
			if($data['blogtitle']!=''){
				//验证没有通过
				if(!$validate->scene('blogtitle')->check($data)){
					$this->error($validate->getError(),'blog/Index/index','',3);
				}
			}
            if($data['host']!=''){
                if(!$validate->scene('host')->check($data)){
                    $this->error($validate->getError(),'blog/Index/index','',3);
                }
            }
//        	filed:显示字段
            $res  = User::where('account', decrypt(cookie("name"),SALT))
                ->update($data);
            if($res!==false){
            	$this->success('修改成功','blog/Index/index');
			}
		}

        //模板变量赋值
        $this->assign('user',$result);

		return view();
	}
	//个人资料
	public function personal(){
        //查询 获取个人信息
        $result = User::where('account',decrypt(cookie("name"),SALT))
            ->field('face,account,nick,email,uid')
            ->find();
        //toArray转换为数组
        $result=$result->toArray();

        //获取表单提交的 所有post数据
        if(request()->isPost()){
        	$data = input('post.');

            // 获取表单上传头像文件 例如上传了001.jpg
            $file = request()->file('face');
            $fileinfo = Base::upload($file,$result['face']);
            if($fileinfo['code']==1){
            	//如果 $fileinfo有值 则赋给$face 如果没有则为空
                $face=$fileinfo['path'];
            }else{
            	$face="";
			}

			//重新组织一个新的数据
        	$newdata=[
        		'uid'=>$result['uid'],
        		'nick'=>$data['nick'],//昵称
				'face'=>$face, //头像
			];
//            验证器 验证昵称不能重复
            $validate = validate('User');
//            新修改的昵称和旧的昵称 不相等则开始验证 相等则跳过验证
            if($result['nick']!==$data['nick']){
                if(!$validate->scene('nick')->check($data)){
                    $this->error($validate->getError(),'blog/Index/personal','',3);
                }
			}

			/*
			 * 模型更新分三种状态
			 * 1：更新成功
			 * 2：数据没有更新 但提交了返回0
			 * 3：更新过程中出现错误 返回false
			 * */

//        	模型 静态更新
            $res = User::update($newdata); //
            if($res!==false){
            	$this->success('修改成功','blog/Index/personal');

			}
		}
//		将查询到的变量分配给模板value值 显示
		$this->assign('personal',$result);

		return view();
	}
	//密码修改
	public function setpasswd(){
		//接收 表单提交数据
		if(request()->isPost()){
			$data = input('post.');
//			使用模型进行数据验证处理
			$result = User::setpasswd($data);
//			dump($result);
			if($result){
				$this->success($result['msg'],'blog/Index/setpasswd');
			}else{
                $this->error($result['msg'],'blog/Index/setpasswd');
			}
		}

		return view();
	}
}
 ?>
