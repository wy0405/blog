<?php
/**
 * Created by PhpStorm.
 * User: 29139
 * Date: 2019/9/22
 * Time: 2:36
 */

namespace app\blog\controller;


use app\common\controller\Base;
use app\common\validate\Picscate as VlidateCate;

class Picscate extends Base
{
    /*
     * 添加分类
     * @return \think\response\View
     * */
    public function add(){
        if(request()->isPost()){
            $data = input('post.');
//            dump($data);die();
            //            实例化验证器
            $vali = new VlidateCate();
                if(!$vali->scene('add')->check($data)){
                    $this->error($vali->getError()); //出错 跳转当前页面
                }
                //获取当前用户的id
            $res = db('user')->where('account',decrypt(cookie("name"),SALT))->find();
                if(!$res){
                    $this->error("添加失败",'blog/Picscate/lst','',1);
                }
                $data['uid']=$res['uid'];
                //过滤表字段
                //方法1：模型中：allowfield(true)
                //方法2：使用unset 去除不存在的字段 这种方式注意顺序放在模型实例前
//            //这一步是为了过滤 上传图片接口
//            if(isset($data['upimg'])){
//                unset($data['upimg']);
//            }
                //方法3：修改config配置项 是否严格检查字段是否存在 'fields_strict'=>false,
//                使用model助手函数 实例化模型进行新增数据
        $modelPicscate = model('picscate');
        // 模型对象赋值
        $modelPicscate->data($data);
        $result = $modelPicscate->allowfield(true)->save();
//        dump($result);die();
            if($result==1){
                $this->success("添加成功",'blog/Picscate/lst','',1);
            }else{
                $this->error("添加失败",'blog/Picscate/lst','',1);
            }

        }
        return view();
    }
    /*
     * 分类列表
     * @return \think\response\View
     * */
    public function lst(){
//        实例化模型
        $modelPicscate = model('picscate');
        // 查询数据集
        $res = $modelPicscate->where("uid",session('uid'))->order('sort', 'desc')
            ->paginate(5);//每页显示5条数据
//        dump($res->toArray());die();
        //分配单个变量到模板
        $this->assign('list',$res);
        return view();

    }
    /*
     * 编辑分类
     * @return \think\response\View
     * */
    public function edit($cid){
        //当时 post提交的数据，1：接收 2：更新入数据库
        //实例化模型
        $cateModel = model('picscate');
        if(request()->isPost()){
            $data = input('post.');
//            dump($data);die();
            //模型中更新和新增都有sava方法，更新方式如下：
            //1. 为sava方法新增第二个参数（更新条件），更新数据中，不能含有主键
            //2. 使用isUpdate 将当前操作设置为更新操作
            $res = $cateModel->allowfield(true)->isUpdate()->save($data);
//            dump($res);die();
            if($res!==false){
//                $this->success("修改成功",'blog/Picscate/lst','',1);
                return json(["code"=>1,"msg"=>"更新成功"]);
            }else{
//                $this->error("修改失败",'blog/Picscate/lst','',1);
                return json(["code"=>0,"msg"=>"更新失败"]);
            }
        }
        //根据$cid查询数据 返回到模板层
        if($cid===null){
            $this->error('参数错误');
        }
        //查询单个数据
        $result = $cateModel->where('cid',$cid)
                  ->find();
//        dump($result->toArray());die();
        $this->assign('cate',$result);

        return view();

    }
    /*
     * 删除分类 单条删除
     * */
    public function del($id){
//        dump($id);
//        dump(is_int($id));die();
        //判断$id是否定义 是不是整型
//        is_int() 函数用于检测变量是否是整数。
//注意: 若想测试一个变量是否是数字或数字字符串（如表单输入，它们通常为字符串），必须使用 is_numeric()。
        if(!isset($id)||!is_numeric($id)){
            return json(['code'=>-1,"msg"=>'操作失败']);
        }
        //实例化模型
        $modelPicscate = model('picscate');
        //使用模型 根据主键删除
        $result=$modelPicscate->where('cid',$id)->delete();
//        dump($result);die();
        if($result==1){
            return json(['code'=>1,"msg"=>'删除成功']);
        }else{
            return json(['code'=>0,"msg"=>'删除失败']);
        }


    }


    /*
     * 排序
     * */
    public function setsort(){
            $cid=input('post.id');
            $sort=input('post.sort');
//            dump($cid);die();
            $modelPicscate=model("picscate");
            // save方法第二个参数为更新条件
            $result=$modelPicscate->save([
                'sort' => $sort,
            ],['cid' => $cid]);
//            dump($result);die();
        if($result==1){
            return json(['code'=>1,"msg"=>'更新成功']);
        }else{
            return json(['code'=>0,"msg"=>'更新失败']);
        }
        }

        /*
         * 上传图片接口
         * */
        public function upimg(){
            //这里要注意，这里需要和文件域的字段名一致
            $file = request()->file('upimg');
            //移动到框架应用根目录/public/uploads/目录下
            if($file){
                $info = $file->move(ROOT_PATH .'public'.DS.'uploads');
                if($info){
                    //成功上传后 获取上传信息
                    //输出 20164342.jpg
//                    echo $info->getSaveName();
                    $data = [
                        "code"=>1,
                        "msg"=>"上传成功",
                        "src"=>$info->getSaveName()//这里的路径可以根据自己的需求修改
                    ];
                }else{
                    //上传失败获取错误信息
//                    echo $file->getError();
                    $data=[
                        "code"=>0,
                        "msg"=>$file->getError()
                    ];
                }
                return json($data);//返回JSON格式数据
            }
        }
}