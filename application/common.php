<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

/*
 * 加密算法
 * 参数1:要加密的变量
 * 参数2：辅助加密的字符串（定义的常量）
 * */
function encrypt($data, $key)
{
    $key    =    md5($key);
    $x        =    0;
    $len    =    strlen($data);
    $l        =    strlen($key);
    $char = "";
    $str = "";
    for ($i = 0; $i < $len; $i++)
    {
        if ($x == $l)
        {
            $x = 0;
        }
        $char .= $key{$x};
        $x++;
    }
    for ($i = 0; $i < $len; $i++)
    {
        $str .= chr(ord($data{$i}) + (ord($char{$i})) % 256);
    }
    return base64_encode($str);
}

/*
 * 解密算法
 * 参数1: 要解密的变量
 * 参数2：辅助解密的字符串（定义的常量）
 *
 * */
function decrypt($data, $key)
{
    $key = md5($key);
    $x = 0;
    $data = base64_decode($data);
    $len = strlen($data);
    $l = strlen($key);
    $char = "";
    $str = "";
    for ($i = 0; $i < $len; $i++)
    {
        if ($x == $l)
        {
            $x = 0;
        }
        $char .= substr($key, $x, 1);
        $x++;
    }
    for ($i = 0; $i < $len; $i++)
    {
        if (ord(substr($data, $i, 1)) < ord(substr($char, $i, 1)))
        {
            $str .= chr((ord(substr($data, $i, 1)) + 256) - ord(substr($char, $i, 1)));
        }
        else
        {
            $str .= chr(ord(substr($data, $i, 1)) - ord(substr($char, $i, 1)));
        }
    }
    return $str;
}

//通过分类id ，取分类名称
/*
 * 参数$type 1.博文分类 2.相册分类  默认博文分类 blogcate模型
 * */
function getcatename($cid,$type=1){
    if($type==1){
        $cate = db('blogcate')->where("cid",$cid)->field("name")->find();
    }else{
        $cate = db('picscate')->where("cid",$cid)->field("name")->find();
    }
    if($cate){
        return $cate['name'];
    }else{
        return "未知";
    }
}

//获取 文章分类名称
function getblogcatename($cid){
    if(!isset($cid)||!is_numeric($cid)){
        return "其它";
    }
    $res = Db('blogcate')->where('cid',$cid)->field("name")->find();
    if($res){
        return $res['name'];
    }else{
        return "其它";
    }

}