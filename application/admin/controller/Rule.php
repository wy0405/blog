<?php

namespace app\admin\controller;
use app\admin\model\AuthRule AS AuthRuleModel;
use think\Controller;
use think\Request;

class Rule extends Controller
{
    /*
     * 权限列表
     * */
    public function index(){
        //使用模型获取所有权限信息 分页
        $list=AuthRuleModel::getRuleall();
        $this->assign('list',$list);
        return view();
    }

    /*
     * 添加权限(加载模板)
     * */
    public function add(){
        $rule = AuthRuleModel::getRuleall();
        $this->assign('rule',$rule);
        return view();
    }

    /*
     * 编辑角色(加载模板)
     * */
    public function edit($id){
        //模型 根据id 获取数据
        $authrule=AuthRuleModel::get($id);
        $this->assign('authrule',$authrule);
        //无限级分类
        $rule = AuthRuleModel::getRuleall();
        $this->assign('rule',$rule);
        return view();
    }
    /*
     * 删除权限
     * */
    public function delete($id){
//        判断是否有子权限 有则不能删除 无则进行下一步删除
        if(AuthRuleModel::checkchild($id)){
            $result = AuthRuleModel::destroy($id);
            if($result>0){
                return returnjson('1','删除成功');
            }
            return returnjson('0','删除失败');
        }else{
            return returnjson('0','该权限有子权限不可删除');
        }

    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request)
    {
        //判断是否有post提交 返回json  由控制器前置方法调用

//        使用方法注入接受POST数据
//        dump($request->post());//获取post变量
        $data = $request->post();
        $result = AuthRuleModel::store($data);
        return $result;

    }

    /*
     * 数据处理
     * */
    public function save(){
        if(!request()->isAjax()){
            return returnjson('0','参数错误');
        }
        //接收参数
        $data = input('post.');
        //模型  更新数据
        $result = AuthRuleModel::store($data);
        if(!$result){
            return returnjson('0','添加失败');
        }else{
            return returnjson('1','添加成功');
        }

    }
}
