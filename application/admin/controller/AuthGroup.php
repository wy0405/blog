<?php
/**
 * Created by PhpStorm.
 * User: 29139
 * Date: 2019/10/24
 * Time: 1:33
 */

namespace app\admin\controller;
use app\admin\model\AuthGroup as AuthGroupModel;
use app\admin\model\AuthRule;
use think\Controller;
use think\Request;

class AuthGroup extends Controller
{
    /*
     * 角色列表
     * */
    public function index(){
        //使用模型获取所有角色信息 分页
        $list=AuthGroupModel::all();
        $this->assign('list',$list);
        return view();
    }

    /*
     * 添加角色(加载模板)
     * */
    public function add(){
        return view();
    }

    /*
     * 编辑角色(加载模板)
     * */
    public function edit($id){
        //模型 根据id 获取数据
        $authgroup=AuthGroupModel::get($id);
        $this->assign('authgroup',$authgroup);
        return view();
    }
    /*
     * 删除角色
     * */
    public function delete($id){
        $result = AuthGroupModel::destroy($id);
        if($result>0){
            return returnjson('1','删除成功');
        }
        return returnjson('0','删除失败');
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request)
    {
        //判断是否有post提交 返回json  由控制器前置方法调用

//        使用方法注入接受POST数据
//        dump($request->post());//获取post变量
        $data = $request->post();
        $result = AuthGroupModel::store($data);
        return $result;

    }

    /*
     * 数据处理
     * */
    public function save(){
        if(!request()->isAjax()){
            return returnjson('0','参数错误');
        }
        //接收参数
        $data = input('post.');
        //模型  更新数据
        $result = AuthGroupModel::store($data);
        if(!$result){
            return returnjson('0','添加失败');
        }else{
            return returnjson('1','添加成功');
        }
    }

    /*
         * 权限分配
         * */
    public function setrule($id){
        //模型 根据id 获取数据
        $authgroup=AuthGroupModel::get($id);
        $this->assign('authgroup',$authgroup);
        return view();
    }

    /*
     * 获取权限列表（用于无限级权限树）
     * */
    public function getRules(){
        $groupid = input("groupid");
        $list = AuthRule::getRule(0,0,$groupid);
        //组织json tree.json 一样的返回数据格式（参考github文档）
        return json(['code'=>0,"msg"=>"获取成功","data"=>["trees"=>$list]]);
    }
}