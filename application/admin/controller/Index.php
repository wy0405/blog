<?php
namespace app\admin\controller;

use app\index\controller\Common;
use think\Db;

class Index extends Common
{
    public function index()
    {
        return $this->fetch();
    }

    public function welcome(){
        //获取当前管理员用户名
        $user = decrypt(cookie("name"),SALT);
        //使用聚会方法count 获取文章数 相册数 会员数
        $count_blog=Db('article')->count();
        $count_pics=Db('pics')->count();
        $count_user=Db('user')->count();
        $count_manager=Db('manager')->count();
        //模板赋值
        return view('', [
            'blogs' => $count_blog,
            'pics' => $count_pics,
            'users' => $count_user,
            'user' =>$user,
            'managers' =>$count_manager
        ]);
    }

}
