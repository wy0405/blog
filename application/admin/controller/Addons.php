<?php

namespace app\admin\controller;

use think\Controller;
use think\Request;
class Addons extends Controller
{
    /**
     * 插件列表
     * @return \think\response\View*
     */
    public function index(){
        //获取插件列表数据 （获取addons目录下的 物理文件）
        $list = get_addon_list();
        $this->assign('list',$list);
//               halt($arr);
        return view();
    }

    //插件安装  name插件名称
    public function install($name){
        //获取插件类名
        $class=get_addon_class($name);
        $result = (new $class)->install();
        if($result===false){
            $this->error("插件安装失败");
        }
        //安装文本编辑器
        //判断是否有静态资源文件
        //思路：判断message目录是否有静态文件夹(static) 有则移动文件
        if(is_dir(ADDON_PATH .$name.DS."static")){
            //将静态资源文件移动到public/static/addons/message
            //注：旧文件目录没有文件会报错 加@ 当报错时跳过报错继续执行
            @rename(ADDON_PATH .$name.DS."static".DS.$name,ROOT_PATH.'public/static/addons'.DS.$name);
        }
        $this->success("插件安装成功");
    }

    //插件卸载  name插件名称
    public function uninstall($name){
        //获取插件类名
        $class=get_addon_class($name);
        $result = (new $class)->uninstall();
        if($result===false){
            $this->error("插件卸载失败");
        }
        //删除文本编辑器 还原静态资源文件
        //思路：判断message目录是否有静态文件夹(static) 有则移动文件
        if(is_dir(ADDON_PATH .$name.DS."static")){
            //还原静态资源文件到message/static/message  跟安装反过来
            //注：旧文件目录没有文件会报错 加@ 当报错时跳过报错继续执行
            @rename(ROOT_PATH.'public/static/addons'.DS.$name,ADDON_PATH .$name.DS."static".DS.$name);
        }
        $this->success("插件卸载成功");

    }
    //离线安装
    public function lxinstall(){
        // 获取表单上传文件 例如上传了001.jpg
        $file = request()->file('addons');
        // 移动到框架应用根目录/public/uploads/ 目录下
        if($file){
            $fileinfo = $file->getInfo();//getInfo()获取上传文件信息
            $result = $this->uzip($fileinfo['tmp_name']);
//            dump($result);die;
            //获取到的 要不是false要不是文件名
            //如果恒等于false时
            if($result===false){
                //自动安装
                return returnjson(0,'安装失败');
            }
            $result_install = $this->install($result);
            if($result_install['code']==1){
                return returnjson('1',"安装成功");
            }
            return returnjson(0,'安装失败');

        }
    }
    //文件解压
    protected function uzip($filename){
        //解压缩
        $zip = new \ZipArchive;
        //要解压的文件
        $zipfile = $filename;
        $res = $zip->open($zipfile);
        if($res!==true){
            return false;
        }
        //要解压到的目录
        $toDir = ADDON_PATH;
        //判断插件目录存不存在 不存在则创建
        if(!file_exists($toDir)) {
            mkdir($toDir,755);
        }
        //获取压缩包中的文件数（含目录）
        $docnum = $zip->numFiles;
//        dump($docnum);
        //定义一个获取文件名变量
        $addonname="";
        //遍历压缩包中的文件
        for($i = 0; $i < $docnum; $i++) {
            $statInfo = $zip->statIndex($i);
            if($statInfo['crc'] == 0) {
                if($i==0){
                    //判断目录是否存在 如果存在返回false 不往下执行
                    if(is_dir($toDir.'/'.substr($statInfo['name'], 0,-1))){
                        return false;
                    }
                    //获取文件名 即目录名
                    $addonname = substr($statInfo['name'], 0,-1);
                }
                //新建目录
                mkdir($toDir.'/'.substr($statInfo['name'], 0,-1));
            } else {
                //拷贝文件
                copy('zip://'.$zipfile.'#'.$statInfo['name'], $toDir.'/'.$statInfo['name']);
            }
        }
        return $addonname;
    }
}
