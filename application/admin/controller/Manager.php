<?php

namespace app\admin\controller;
use app\admin\model\Manager as ManageModel;
use think\Request;
class Manager extends Common
{
    //定义一个控制器 前置操作
    protected $beforeActionList = [
        //except表示这些方法不使用前置方法
        //表示只有这些方法使用前置方法。
        'checkrequst' => ['only'=>'save,update,delete,setstatus'],
    ];
    /**
     * 管理员列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //使用模型获取管理员信息 分页
        $list=ManageModel::getlistall();
        $this->assign('list',$list);
        return view();
    }

    /**
     * 创建添加管理员表单页.
     *
     * @return \think\Response
     */
    public function add()
    {
        return view();
    }

    /**
     * 添加管理员
     *
     * @param  \think\Request  $request
     * @return \think\Response
     */
    public function save(Request $request)
    {
        //判断是否有post提交 返回json  由控制器前置方法调用

        //        使用方法注入接受POST数据
//        dump($request->post());//获取post变量
        $data = $request->post();
        //向模型提交数据
        $result = ManageModel::store($data);
        return $result;
    }

    /**
     * 编辑管理员表单页.
     *
     * @param  int  $id
     * @return \think\Response
     */
    // 获取数据传到模板上
    public function edit($id)
    {
//        $manager=Db('manager')->where('id',$id)->find();
        $manager=ManageModel::get($id);//模型对象支持数组方式访问
        $this->assign('manager',$manager);
        return view();
    }

    /**
     * 保存更新的资源
     *
     * @param  \think\Request  $request
     * @param  int  $id
     * @return \think\Response
     */
    public function update(Request $request)
    {
        //判断是否有post提交 返回json  由控制器前置方法调用

//        使用方法注入接受POST数据
//        dump($request->post());//获取post变量
        $data = $request->post();
        $result = ManageModel::store($data);
        return $result;

    }

    /**
     * 删除指定资源
     *
     * @param  int  $id
     * @return \think\Response
     */
    public function delete($id)
    {
        //判断是否有post提交 返回json  由控制器前置方法调用

        $result = ManageModel::delmanager($id);
        return $result;

    }


    //设置管理员状态
    public function setstatus($id){
        //判断是否有post提交 返回json  由控制器前置方法调用

        $result = ManageModel::setstatusmanager($id);
        return $result;

    }
    /**
     *  私有化方法
     *  判断是否有post提交
     *  返回json数据
     */
    protected function checkrequst(){
        if(!request()->isPost()){
//            return json(['code'=>0,'msg'=>"操作异常"]);
//            echo returnjson('0','操作异常'); echo不支持 json对象 需要用函数转换
            echo json_encode(['code'=>0,'msg'=>'操作异常']);exit;
        }
    }
}
