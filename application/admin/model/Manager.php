<?php

namespace app\admin\model;
use think\Model;

class Manager extends Model
{
    //设置数据集返回类型
    protected $resultSetType = 'collection';
    // 开启自动写入时间戳字段
    protected $autoWriteTimestamp =true;      //默认创建时间字段为 create_time ， 更新时间字段为 update_time
    //获取所有管理员数据
    public static function getlistall(){
        //在模型内 使用self 调取自身模型进行查询
        $list=self::all(function($query){
            //加条件使用闭包函数
            $query->order('id', 'asc');
        })->toArray();
        return $list;
    }

    //添加 和 修改 管理员 信息
    public static function store($data){
        //判断提交过来的$data 是否有id 有则是修改 没有id则是添加
        if(isset($data['id'])){
            $scene  = 'edit';
            $msg    = "修改";
            $action = "update";
        }else{
            $scene  = 'add';
            $msg    = '添加';
            $action = "create";
        }
        //验证数据合法性
        $valiManager=validate("Manager");
        if(!$valiManager->scene($scene)->check($data)){
//            return ['code'=>0,'msg'=>$valiManager->getError()];
            return returnjson('0',$valiManager->getError());
        }
//        写入数据库 repasswd字段 数据表里面没有 这里要清除
        unset($data['repasswd']);
        $result = self::$action($data);
        if(!$result){
//            return ['code'=>0,'msg'=>"{$msg}失败"];
            return returnjson(0,"{$msg}失败");
        }
//        return ['code'=>1,'msg'=>"{$msg}成功"];
        return returnjson(1,"{$msg}成功");


    }

    //修改管理员信息
//    public static function updatemanager($data){
//        //验证数据合法性
//        $valiManager=validate("Manager");
//        if(!$valiManager->scene('edit')->check($data)){
//            return json(['code'=>0,'msg'=>$valiManager->getError()]);
//        }
////        导入数据库 repasswd字段 数据表里面没有 这里要清除
//        unset($data['repasswd']);
//        $data['update_time']=time();
//        //如果数据中包含主键， 可以直接使用：update()
//        $result =self::update($data);
//        if(!$result){
//            return ['code'=>0,'msg'=>"修改失败"];
//        }
//        return ['code'=>1,'msg'=>"修改成功"];
//    }

    //删除管理员
    public static function delmanager($id){
//        $result = Db('manager')->delete($id);
        $result = self::destroy($id);
        if($result){
//            return ['code'=>1,'msg'=>'删除成功'];
            return returnjson(1,"删除成功");
        }
//        return ['code'=>0,'msg'=>'删除失败'];
        return returnjson(0,"删除失败");
    }
    //设置管理员状态
    public static function setstatusmanager($id){
        //使用模型获取单条数据
//        $status = Db("manager")->where("id",$id)->value("status");
        $status = self::where('id',$id)->value('status');
        $data['status']=1; //默认等于1
        $msg="已停用";
        if($status==0){
            $data['status']=1;
            $msg="已启用";
        }else{
            $data['status']=0;
            $msg="已停用";
        }
//        $result = Db("manager")->where('id',$id)->update($data);
        $result = self::where('id',$id)->update($data);
        if($result){
//            return ['code'=>1,'msg'=>$msg];
            return returnjson('1',"$msg");
        }
//        return ['code'=>0,'msg'=>'操作失败'];
        return returnjson('0',"操作失败");
    }
}
