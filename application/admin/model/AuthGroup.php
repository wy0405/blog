<?php
/**
 * Created by PhpStorm.
 * User: 29139
 * Date: 2019/10/24
 * Time: 1:42
 */

namespace app\admin\model;


use think\Model;

class AuthGroup extends Model
{
    //添加 和 修改 管理员 信息
    public static function store($data){
        //判断提交过来的$data 是否有id 有则是修改 没有id则是添加
        if(isset($data['id'])){
            $scene  = 'edit';
            $msg    = "修改";
            $action = "update";
        }else{
            $scene  = 'add';
            $msg    = '添加';
            $action = "create";
        }
        //验证数据合法性
//        $valiManager=validate("Manager");
//        if(!$valiManager->scene($scene)->check($data)){
//            return returnjson('0',$valiManager->getError());
//        }

        //判断有没有setrule这个隐藏域有则往下执行
        if(isset($data['setrule'])){
            //当setrule=1为权限配置操作
            if($data['setrule']==1){
                //判断data里是否有rules数组 有则转换为字符串
                if(isset($data['rules'])){
                    $data['rules']=implode(",",$data['rules']);
                }else{
                    $data['rules']="";
                }
            }
        }



//        写入数据库 repasswd字段 数据表里面没有 这里要清除
        unset($data['repasswd']);
        unset($data['setrule']);
        $result = self::$action($data);
        if(!$result){
            return returnjson(0,"{$msg}失败");
        }
        return returnjson(1,"{$msg}成功");


    }

}