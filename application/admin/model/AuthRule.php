<?php

namespace app\admin\model;

use think\Model;

class AuthRule extends Model
{
    //添加 和 修改 管理员 信息
    public static function store($data){
        //判断提交过来的$data 是否有id 有则是修改 没有id则是添加
        if(isset($data['id'])){
            $scene  = 'edit';
            $msg    = "修改";
            $action = "update";
        }else{
            $scene  = 'add';
            $msg    = '添加';
            $action = "create";
        }
        //验证数据合法性
//        $valiManager=validate("Manager");
//        if(!$valiManager->scene($scene)->check($data)){
//            return returnjson('0',$valiManager->getError());
//        }
//        写入数据库 repasswd字段 数据表里面没有 这里要清除
        unset($data['repasswd']);
        $result = self::$action($data);
        if(!$result){
            return returnjson(0,"{$msg}失败");
        }
        return returnjson(1,"{$msg}成功");


    }

    public static function getRuleall($pid=0,$level=0){
        $res = self::where('pid',$pid)->select();
        static $arr =[];
        foreach ($res as $v){
            $v['level']=$level;
            $arr[]=$v;
            self::getRuleall($v['id'],$level+1);
        }
        return $arr;
    }
    //判断是否有子权限
    public static function checkchild($pid){
        $res = self::get(['pid'=>$pid]);
//        dump($res);
        if($res){
            return false;
        }
        return true;
    }
    /**
     * 获取权限列表 无限级分类 （用于无限级权限树）
     * @param int $pid
     * @param int $level
     * @return array
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public static function getRule($pid=0,$level=0,$groupid){
        $rules = Db("auth_group")->where("id",$groupid)->field("rules")->find();
        //将数组拆分成字符串
        $rules=explode(",",$rules['rules']);
        $res = self::where('pid',$pid)->select();
        $arr =[];
        foreach ($res as $v){
            $tmp=[];//临时数组 组织数据
            $tmp['name']=$v['title'];
            $tmp['value']=$v['id'];
            $tmp['checked']=false;//默认为false 满足下面条件则为true
            //$rules 是不是个数组
            if(is_array($rules)){
                //$rules 里面存在 $v['id']
                if(in_array($v['id'],$rules)){
                    $tmp['checked']=true;
                }
            }
            $tmp['list']=self::getRule($v['id'],$level+1,$groupid);
            $arr[] = $tmp;
        }
        return $arr;

    }
}
