<?php
/**
 * Created by PhpStorm.
 * User: 29139
 * Date: 2019/10/13
 * Time: 4:06
 */

namespace app\admin\validate;


use think\Validate;

class Manager extends Validate
{
    //定义规则
    protected $rule = [
        'id'=>'require',
        'account' => 'require|min:5|unique:manager',
        'passwd' => 'require|min:6|max:16',
        'repasswd' => 'require|confirm:passwd',
    ];
    //提示信息
    protected $message = [
        'id.require' => 'ID不能为空',
        'account.require' => '管理员帐号不能为空',
        'account.unique' => '该管理员帐号已注册',
        'account.min' => '密码要大于5位',
        'passwd.require' => '密码不能为空',
        'passwd.min' => '密码要大于6位',
        'passwd.max' => '密码要小于16位',
        'repasswd.require' => '确认密码不能为空',
        'repasswd.confirm' => '确认密码和第一次密码不一致',
    ];
    //场景验证
    protected $scene = [
        'add' => ['account','passwd','repasswd'],
        'edit' => ['id','passwd','repasswd'],
    ];

}