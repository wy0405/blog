<?php
/**
 * Created by PhpStorm.
 * User: 29139
 * Date: 2019/9/16
 * Time: 17:59
 * 行为类
 */

namespace app\admin\behavior;
use app\common\controller\Base;


class Login extends Base
{
    //定义行为入口方法
    public function run(&$params)
    {
        // 行为逻辑


    }
//    没有其他方法会默认走run方法
//    blog 所有控制器都会执行到这里
    public function actionBegin(&$params)
    {
        $status = Base::isLogin();//大驼峰命名法 相当于 调用is_login 方法
//        判断登录状态
        if(!$status){
//            继承Base才能调用
            $this->redirect('index/Index/index'); //未登录则跳转
        }

    }

}