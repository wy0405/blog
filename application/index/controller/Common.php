<?php

namespace app\index\controller;

use app\common\controller\Base;
use think\Controller;
use think\Db;
use think\Request;

class Common extends Base
{
    /**
     * 获取今日推荐数据
     * @param int $num
     * @return array
     */
    protected function gettuijian($num=1,$fiels="aid,title"){
        $map=[];//未登录时为空数组
        //判断登录来设置查询条件
        if(Base::isLogin()){
            $cids = Base::getcurusercates();
            $map=['cid'=>array('in',$cids)];
        }
        $list=Db('article')->whereTime('addtime', 'addtime')->where($map)->limit($num)->field($fiels)->select();
//        dump($list);die;
        return $list;
    }
    /**
     * 获取博文分类
     * @param int $num
     * @return array
     */
    protected function getblogcates($num){
        $map=[];//未登录时为空数组
        //判断登录来设置查询条件
        if(Base::isLogin()){
            $cids = $this->getcurusercates();
            $map=['cid'=>array('in',$cids)];
        }
        $list=Db('blogcate')->where($map)->order('sort Asc')->limit($num)->select();
//        dump($list);die;
        return $list;
    }
    /**
     * 获取最新博文
     * @param int $num
     * @return array
     */
    protected function getnewblogs($num=4){
        $map=[];//未登录时为空数组
        //判断登录来设置查询条件
        if(Base::isLogin()){
            $cids = Base::getcurusercates();
            $map=['cid'=>array('in',$cids)];
        }
        $list=Db('article')->where($map)->order('addtime Desc')->limit($num)->select();
        return $list;
    }

    /**
     * 获取所有博文（分页）
     * @param int $num
     * @return array
     */
    protected function getnewblogss($limits=10){
        $map=[];//未登录时为空数组
        //判断登录来设置查询条件
        if(Base::isLogin()){
            $cids = Base::getcurusercates();
            $map=['cid'=>array('in',$cids)];
        }
        $list=Db('article')->where($map)->order('cid Desc')->paginate($limits);
        return $list;
    }

    /**
     * 获取热门博文
     * @param int $num     显示条数
     * @param int $hotnum  阅读人数
     * @return array
     */
    protected function gethotblogs($num=1,$hotnum=1){
        $map=[];//未登录时为空数组
        //判断登录来设置查询条件
        if(Base::isLogin()){
            $cids = Base::getcurusercates();
            $map=['cid'=>array('in',$cids)];
        }
        $list=Db('article')->where('views',">",$hotnum)->where($map)->order('views asc')->limit($num)->select();
        return $list;
    }

    /**
     * 获取相关阅读（根据添加时间）
     * 获取 $limits 条数据
     * @return list
     */
    protected function getxiangguan($limits){
        $map=[];//未登录时为空数组
        //判断登录来设置查询条件
        if(Base::isLogin()){
            $cids = Base::getcurusercates();
            $map=['cid'=>array('in',$cids)];
        }
        $list=Db('article')->where($map)->field('aid,title')->order('addtime acs')->limit($limits)->select();
        return $list;
    }





}
