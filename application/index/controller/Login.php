<?php
namespace app\index\controller;

use app\common\controller\Base;
use app\common\model\User;
use think\Controller;
use myqq\Qc;
use think\Db;

class Login extends Base
{

    //登录界面
    public function index()
    {
        // TP 行为标签位  登录状态验证
        if(Base::isLogin()){
            $this->redirect('blog/index/index');//cookie 值存在则跳转
        }
        //判断有没有post提交
        if(request()->isAjax())
        {
            $data=input('post.','','trim'); //参数1 post. "."是获取所有  filer过滤 空格trim
            $res = User::login($data); //通过模型获取数据
//            dump($res);
            //返回注册状态
            return json($res);


        }

        return view();
    }
    //注册界面
    public function reg()
    {
        //判断有没有post提交
        if(request()->isAjax()){
            //使用TP方法input 接收post数据 过滤trim空格
            $data=input('post.','','trim');
//            dump(Db('user')->select());
            $result=User::userreg($data);
            //返回注册状态
            return json($result);
        }
        return view();
    }
    //激活帐号方法
    public function active($token){
        //思路:判断$token存不存在 存在就激活 不存在就跳转回登录页面
        if(isset($token)){
            $res = User::active($token);
            if($res['code']==1){
                $this->success($res['msg'],'index/login/index'); //跳转登录页面
            }else{
                $this->success($res['msg'],'index/login/reg');//跳转注册页面
            }

        }
    }
    //  QQ第三方登录
    public function qqlogin(){
        $qc = new Qc();
        $qc->qq_login();
    }
    //QQ授权登录 回调地址  审核通过后把回调地址改成 index/Login/qqcallback   该方法用来接收回调信息
    public function qqcallback(){
//        echo input('code');//获取code
        $qc = new Qc();
        $access_token = $qc->qq_callback();
        $openid = $qc->get_openid();//当前登录用户唯一标识
        //重新实例化qc
        $qc = new Qc($access_token,$openid);
//        dump($qc->get_user_info);
        $userinfo = $qc->get_user_info();
        $data=[
            'userkey'=>$openid,
            'nickname'=>$userinfo['nickname'],
            'faceimg'=>$userinfo['figureurl_l'],
        ];
        //判断用户是否为老用户
        $qquser=Db('users')->where('userkey',$openid)->find();
 // 审核通过后 调回去学好

    }
    /*
     * 退出登录 （清除cookie）
     * */
    public function goout(){
        //清除session
        session('uid', null);
        // 助手函数 删除cookie
        cookie('name', null);
        cookie('code', null);
        $this->redirect('index/index/index');

    }

}
