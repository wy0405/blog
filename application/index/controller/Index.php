<?php

namespace app\index\controller;

class Index extends Common
{
    //控制器初始化 获取热门博文
    public function _initialize()
    {
        //获取热门文章
        $hotblogs = $this->gethotblogs(1,4);
        $this->assign('hotblogs',$hotblogs);
    }
    /**
     * 博客首页
     *
     * @return \think\Response
     */
    public function index()
    {
        // 获取今日推荐
        /**
         * @package num 查询数据条数 (int)
         * @package fiels 查询字段（string）
         */
        $tuijian = $this->gettuijian(1,"aid,title,remark");
        $this->assign('tuijian',$tuijian);

        //博文分类
        $blogcates = $this->getblogcates(5);
        $this->assign('blogcates',$blogcates);

        //获取最新博文
        $newblog = $this->getnewblogs(10);
        $this->assign('newblog',$newblog);

        //获取热门文章
        $hotblogs = $this->gethotblogs(1,4);
        $this->assign('hotblogs',$hotblogs);
        return view();
    }
    //关于我们
    public function about(){}

}
