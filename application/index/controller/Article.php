<?php

namespace app\index\controller;

use think\Controller;
use think\Request;
use app\index\model\Article as ArticleModel;
class Article extends Common
{
    //控制器初始化 获取热门博文
    public function _initialize()
    {
        //获取热门文章
        $hotblogs = $this->gethotblogs(1,4);
        $this->assign('hotblogs',$hotblogs);
    }
    /**
     * 博文列表
     *
     * @return \think\Response
     */
    public function index()
    {
        //获取博文分类列表
        $blogcates = Db("blogcate")->order("sort desc")->field('cid,name')->select();
//        dump($picscates);
        $this->assign("blogcates",$blogcates);
        //获取所有博文 分页
        $list = $this->getnewblogss(4); //6页
        // 把分页数据赋值给模板变量list
        $this->assign('list', $list);
        // 渲染模板输出

        return view();
    }

    /**
     * 博文详情
     * @param $aid
     */
    public function show($aid){
//        点击浏览次数
        Db('article')->where('aid',$aid)->setInc('views');
        //根据当前aid 获取 数据cid
        $info = db("article")->where('aid',$aid)->field('cid,title')->find();
//        dump($info);
        //        echo "博文详情$aid";
        //数据传到模板
        $this->assign("article",$info);
        //获取热门文章
        $hotblogs = $this->gethotblogs(1,4);
        $this->assign('hotblogs',$hotblogs);

        //获取博文详情内容
        $content = Db('article')->where('aid',$aid)->find();
        $this->assign('content',$content);

        //获取 2条 相关阅读信息
        $xgread = $this->getxiangguan(2);
        $this->assign('xgread',$xgread);
        return view();

    }



}
