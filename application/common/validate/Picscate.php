<?php
/**
 * Created by PhpStorm.
 * User: 29139
 * Date: 2019/9/23
 * Time: 1:27
 */

namespace app\common\validate;


use think\Validate;

class Picscate extends Validate
{
    //定义规则
    protected $rule = [
        'name' => 'require',
        'sort' => 'require|number',
    ];
    //提示信息
    protected $message = [
        'name.require' => '分类名称不能为空',
        'sort.require' => '排序不能为空',
        'sort.number' => '排序必须是数字',
    ];
    //场景验证
    protected $scene = [
        'add' => ['name','sort'],
    ];

}