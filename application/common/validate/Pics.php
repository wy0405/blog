<?php
/**
 * Created by PhpStorm.
 * User: 29139
 * Date: 2019/9/23
 * Time: 1:27
 */

namespace app\common\validate;


use think\Validate;

class Pics extends Validate
{
    //定义规则
    protected $rule = [
        'title' => 'require',
    ];
    //提示信息
    protected $message = [
        'title.require' => '分类名称不能为空',
    ];
    //场景验证
    protected $scene = [
        'add' => ['title'],
    ];

}