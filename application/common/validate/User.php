<?php
/**
 * Created by PhpStorm.
 * User: 29139
 * Date: 2019/8/25
 * Time: 1:12
 */

namespace app\common\validate;
use think\Validate;

class User extends Validate
{
    //定义规则
    protected $rule = [
        'account' => 'require|unique:user',
        'passwd' => 'require|min:6',
        'repasswd' => 'require|confirm:passwd',
        'email' => 'require|email|unique:user',
        'code|验证码'=>'require|captcha',
        'blogtitle' => 'unique:user', //在user表中不重复
        'host' => 'unique:user',
        'nick' => 'unique:user',
        'oldpasswd' => 'require|min:6',

    ];
    //提示信息
    protected $message = [
        'account.require' => '帐号不能为空',
        'account.unique' => '该帐号已注册',
        'passwd.require' => '密码不能为空',
        'passwd.min' => '密码要大于6位',
        'repasswd.require' => '确认密码不能为空',
        'repasswd.confirm' => '确认密码和第一次密码不一致',
        'email.require' => '邮箱不能为空',
        'email.unique:user' => '邮箱已存在',
        'email.email' => '邮箱格式错误',
        'code.require'=> '请输入验证码',
        'code.captcha'=> '验证码错误',
        'blogtitle.unique' => '博客标题已存在',
        'host.unique' => '博客域名已存在',
        'nick' => '昵称已存在',
        'oldpasswd.require' => '密码不能为空',
        'oldpasswd.min' => '密码要大于6位',
    ];
    //场景验证
    protected $scene = [
        'reg' => ['account','passwd','repasswd','email','code'],
        'index' => ['account'=>'require','passwd'=>'require'],
        'blogtitle' => ['blogtitle'],
        'host' => ['host'],
        'nick' => ['nick'],
        'passwd'=>['oldpasswd','passwd','repasswd'],


    ];

}