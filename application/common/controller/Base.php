<?php
/**
 * Created by PhpStorm.
 * User: 29139
 * Date: 2019/8/28
 * Time: 12:31
 * 公共控制器
 */

namespace app\common\controller;
use PHPMailer\PHPMailer\PHPMailer;
use think\Controller;
use think\Cookie;
class Base extends Controller
{
    //控制器初始化
    public function _initialize()
    {
        //把获取的留言菜单分配到模板
        $addonmenu = $this->getaddonsmenu();
        $this->assign('addonmenu',$addonmenu);
    }

/*
 * $toemail 用户注册邮箱
 * string $title 激活邮件标题
 * $account 激活帐号
 * $url 激活链接地址
 * */

    //$title="帐号激活"可选参数有初始值放最后面才可以传值
    public static function sendTextmail($toemail,$account,$url,$title="帐号激活"){
        $mail = new PHPMailer(true);

        try {
            //Server settings
            $mail->SMTPDebug = 0;                                       // Enable verbose debug output
            $mail->isSMTP();                                            // Set mailer to use SMTP
            $mail->Host       = 'smtp.163.com';  // Specify main and backup SMTP servers  SMTP服务器
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   = '15916477289@163.com';                     // SMTP username
            $mail->Password   = 'czyueU3611';                               // SMTP password 客户端授权密码
            $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
            $mail->Port       = 25;                                    // TCP port to connect to
            $mail->CharSet='UTF-8';                                    //邮件编码

            //Recipients
            $mail->setFrom('15916477289@163.com', '博客');
            $mail->addAddress($toemail);               // Name is optional 邮箱接收
            $mail->addReplyTo('15916477289@163.com','博客'); //回复邮件地址

            // Content 邮件内容
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $title;//邮件标题
            $mail->Body    = "您好!<br>感谢您注册博客系统，<br>你的用户名为:{$account}，请点击以下链接激活帐号，<a href='{$url}' target='_blank'>{$url}</a> <br>如果以上链接无法点击，请将上面的地址复制到您的浏览器地址栏中进入博客激活帐号<br>"; //主体内容
//            $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';  //内容属性

            $mail->send();
//            echo '邮件发送成功';
        } catch (Exception $e) {
            echo "邮件发送失败: {$mail->ErrorInfo}";
        }

    }

    //判断登录状态
    //静态方法 不能用this 对象
    public static function isLogin(){
        // has 判断指定cookie值是否存在
//        name和code有一个不存在就返回false
        if(!Cookie::has('name')|| !Cookie::has('code')){
            return false;  //不存在
        }
        if(cookie('code') == md5(decrypt(cookie('name'),SALT).SALT)){
            return true;
        }
        return false;
    }
    //设置登录状态
    public static function setLogin($account,$uid){

        // 设置
        //session设置用户uid （博客列表根据当前用户uid获取分类等会使用）
        session('uid',$uid);
        //使用cookie 记录 解密过的账号名
        cookie('name',encrypt($account,SALT), 3600); //帐号用加密算法 加密
        //cookie安全 再定义个cookie
        cookie('code',md5($account.SALT),3600);

        return ['code'=>1,'msg'=>"登录成功",'name'=>$account];

    }


    // 文件上传类
    //    参数 $dir 变量目录 为了以后其他图片上传 扩展用
    //    $oldfile 用来实现删除旧文件
    public static function upload($file,$oldfile=null,$dir='uploads'){

// 移动到框架应用根目录/public/uploads/ 目录下
        if($file){
            $info = $file->move(ROOT_PATH . 'public' . DS . $dir);
            if($info){
// 成功上传后 获取上传信息
                if($oldfile!==null){
                    //组合路径  项目根目录/public/$oldfile
                    $path=ROOT_PATH.'public/'.$oldfile;
                    //删除前 判断是不是文件 如果 是 并且文件存在 则删除之前文件
                    if(is_file($path) && file_exists($path)){
                        @unlink($path);// @ 符号 删除出现异常时 还是会继续进行
                    }
                }

// 输出 20160820/42a79759f284b767dfcb2a0197904287.jpg
                return ['code'=>1,'msg'=>'上传成功','path'=>$dir.DS.$info->getSaveName()];

            }else{
// 上传失败获取错误信息
                return ['code'=>0,'msg'=>$file->getError()];
            }
        }
    }

    //获取当前用户所有分类id
    protected static function getcurusercates(){
        //1、获取用户id
        $accout=decrypt(cookie("name"),SALT); //获取cookie name 需要解密
        //2、通过用户名查询用户id
        $uid=Db('user')->where('account',$accout)->value('uid');
        //3.通过用户uid获取该用户所有分类
        $cate = Db('blogcate')->where('uid',$uid)->column('cid');
        return $cate;
    }

    /**
     * 获取已安装插件的菜单项
     * @param int $type
     * @return mixed
     */
    public function getaddonsmenu($type=0){
        $addons=get_addon_list();
        $arr=[];
        foreach ($addons as $v){
            //判断插件是否安装 判断插件目录是否存在config文件
            if(is_file(ADDON_PATH .$v['name'].DS.'config.php')){
                //判断菜单配置项是否存在 并且 不能为空
                if(isset($v['menu'])&&!empty($v['menu'])){
                    if($type==$v['menu']['type'])
                    //条件成立 存入数组
                    $arr[] = $v['menu'];
                }
            }
        }
//        dump($arr);
        return $arr;
    }


}