<?php
/**
 * Created by PhpStorm.
 * User: 29139
 * Date: 2019/8/21
 * Time: 1:07
 */

namespace app\common\model;
//引入User跟现有类名冲突 取个别名 来调用
use app\common\validate\User as UserValidate;
use app\common\controller\Base;
use think\cache\driver\Redis;
use think\Model;
use think\Db;

class User extends model
{
    protected $insert=['regtime','salt','token','token_exptime'];
    //md5加密 模型修改器   格式：set(字段名首字母大写)Attr
    protected function setPasswdAttr($value)
    {
        return md5($value.md5(time()));
    }
    //自动完成注册时间
    protected function setRegtimeAttr(){
        return time();
    }
    //自动完成辅助密码
    protected function setSaltAttr(){
        return md5(time());
    }
    //自动完成 生成邮箱激活码
    protected function setTokenAttr($value,$data){ //$value值没意义，要的是$data值
        $token=md5($data['account'].$data['passwd'].time());
        return $token;
    }
    //自动完成 生成邮箱激活码失效时间
    protected function setTokenExptimeAttr(){  //下划线_ 去掉 改成大写
        $token_exptime=time()+60*60*24; //获取当前时间戳 加上 24小时
        return $token_exptime;
    }


    //用户注册
    public static function userreg($data){
        //使用验证器
        $userValidate = new UserValidate();
        if(!$userValidate->scene('reg')->check($data)){
            return ['code'=>0,'msg'=>$userValidate->getError()];
        }
//        将获取到的表单 使用模型静态方法添加数据
//  第二个参数 ：true表示将所有数据对应写入数据库字段内 repasswd 不会写入字段
        $res = self::create($data,true);
        if($res){
           // dump($res);die();
            //发送邮箱验证
            $result=Base::sendTextmail($res->email,$res->account,url('index/Login/active','','',true)."?token=".$res->token);
            return ['code'=>1,'msg'=>'注册成功,请到激活邮箱中激活帐号！'];
        }else{
            return ['code'=>0,'msg'=>'注册失败'];
        }
    }
    //用户激活


    public static function active($token){
        //我们在当前模型 所以使用self获取当前模型而不是User
        //下方重新发送邮件 需要获取表单信息
        $res = self::field('token,token_exptime,account,passwd,email')->where('token',$token)->find();
        if(!$res){
            return['code'=>0,'msg'=>'用户不存在，请注册'];
        }
        if(time()<$res['token_exptime']){
            $res->isactive=1;//更新数据库 字段isactive[激活状态]
            $result=$res->save();//调用save方法保存
//            dump($result);
            if($result ===0 ){
                //养成一个习惯 返回成功状态码 提示信息 前端控制器 拿code 显示提示信息
                return['code'=>1,'msg'=>'帐号已激活，请登录'];
            }
            if($result ===1 ){
                return['code'=>1,'msg'=>'激活成功，请登录'];
            }
            if($result ===false ){
                return['code'=>0,'msg'=>'激活失败'];
            }
        }
        //激活链接有效期已过的时候重新给用户发邮件 (重新定义)
        $newtoken=md5($res->account.$res->passwd.time());
        $time=time()+60*60*24;
        //因为上面运用了数据完成 所以这里不可使用save()方法 默认会走修改器 所以用Db助手函数来写查询
        db('user')->where('token'.$token)->update(['token'=>$newtoken,'token_exptime'=>$time]);
        $result=Base::sendTextmail($res->email,$res->account,url('index/Login/active','','',true)."?token=".$res->token);
        return['code'=>0,'msg'=>'链接已失效，新的链接已经发送到您的邮箱'];

    }
    // 用户登录
    public static function login($data){
        //开启计数器
        $redis  = new Redis();
        $username = $data['account'];
//        dump($username);exit();
        $numData = $redis->get($username);
        if($numData >= 3 ){
            return ['code'=>0,'msg'=>"登录次数过多，请稍后尝试"];
            exit();
        }

        //验证器
        $userValidate = new UserValidate();
        if(!$userValidate->scene('index')->check($data)){
            return ['code'=>0,'msg'=>$userValidate->getError()];
        }
//        查询 id account,passwd,salt,isactive 字段  find查询
        $res = self::where('account',$data['account'])->field('uid,account,passwd,salt,isactive')->find();

        if(!$res){
            $redis->inc($username); //失败 自增
            $handler = $redis->handler();  //返回句柄对象，可执行其它高级方法
            $handler->expire($username,20);  //设置过期时间 20秒
            return ['code'=>0,'msg'=>'用户不存在'];

        }
        if($res['isactive']==0){
            return ['code'=>0,'msg'=>"用户未激活，请到注册邮箱中激活"];
        }
//        判断密码
        if(md5($data['passwd'].$res['salt'])!=$res['passwd']){
            $redis->inc($username); //失败 自增
            $handler = $redis->handler();  //返回句柄对象，可执行其它高级方法
            $handler->expire($username,20);  //设置过期时间 20秒
            return ['code'=>0,'msg'=>"密码输入错误"];
        }
//        设置用户登录状态
        return Base::setLogin($data['account'],$res['uid']);
    }

    //密码修改
    public static function setpasswd($data){
        $userValidate = new UserValidate();
        if(!$userValidate->scene('passwd')->check($data)){
            return ['code'=>0,'msg'=>$userValidate->getError()];
        }
//        使用模型静态方法 获取user的数据 查询cookie name 里帐号的所有字段 就是数据库里的旧密码 返回一行数据
        $user = self::get(['account'=>decrypt(cookie("name"),SALT)]);
//        $data是表单新提交的数据密码  $user是数据库里的之前保存的数据密码
        //表单的“旧密码”+ 旧数据 “时间戳” 不等于 数据库里旧密码时报错
        if(md5($data['oldpasswd'].$user['salt'])!=$user['passwd']){
            return ['code'=>0,'msg'=>'旧密码输入不正确'];
        }
        $user->passwd=$data['passwd']; //会走上面的修改器 自动MD5加密
        $user->salt="";
        $res = $user->save();
        if($res){
            return ['code'=>1,'msg'=>"密码修改成功"];
        }

    }


}