/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : test_blog

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2019-08-22 09:31:05
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for td_message
-- ----------------------------
DROP TABLE IF EXISTS `td_message`;
CREATE TABLE `td_message` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '0' COMMENT '姓名',
  `phone` varchar(11) NOT NULL DEFAULT '0' COMMENT '手机号',
  `content` varchar(200) NOT NULL DEFAULT '0' COMMENT '留言内容',
  `create_time` int(10) NOT NULL DEFAULT '0',
  `update_time` int(10) NOT NULL DEFAULT '0',
  `touid` int(10) NOT NULL DEFAULT '0' COMMENT '留言对象',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='留言表';

-- ----------------------------
-- Records of td_message
-- ----------------------------
INSERT INTO `td_message` VALUES ('1', '34535', '543535', '<p>353535</p>', '1540052909', '1540052909', '2');

-- ----------------------------
-- Table structure for yf_article
-- ----------------------------
DROP TABLE IF EXISTS `yf_article`;
CREATE TABLE `yf_article` (
  `aid` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL COMMENT '标题',
  `keyword` varchar(100) DEFAULT NULL COMMENT '关键字',
  `desc` varchar(150) DEFAULT NULL COMMENT '描述',
  `remark` varchar(150) DEFAULT NULL COMMENT '摘要',
  `content` text COMMENT '内容',
  `cid` int(11) NOT NULL COMMENT '分类ID',
  `pic` varchar(150) DEFAULT NULL COMMENT '图片',
  `source` varchar(45) DEFAULT NULL COMMENT '来源',
  `addtime` int(10) DEFAULT NULL COMMENT '发布时间',
  `views` int(11) DEFAULT '0' COMMENT '浏览次数',
  `ischeck` tinyint(1) DEFAULT '0' COMMENT '0：审核不通过，1：审核通过',
  `isnominate` tinyint(1) DEFAULT '0' COMMENT '0：不推荐 ，1：推荐',
  `istop` tinyint(1) DEFAULT '0' COMMENT '0：不置项，1：置顶',
  PRIMARY KEY (`aid`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='文章表';

-- ----------------------------
-- Records of yf_article
-- ----------------------------
INSERT INTO `yf_article` VALUES ('5', '基于HTML5的云虚拟主机配置界面', 'HTML5', '云计算大行其道，上期开源中国的原创会就有好几位云计算专家演讲，从底层的虚拟化技术，到上', '云计算大行其道，上期开源中国的原创会就有好几位云计算专家演讲，从底层的虚拟化技术，到上层的云存储和应用API，耳濡目染，也受益匪浅，算是大势所趋，回头看看Qunee组件，借这个趋势，可以在云计算可视化上发挥作用', '<p>云计算大行其道，上期开源中国的原创会就有好几位云计算专家演讲，从底层的虚拟化技术，到上层的云存储和应用API，耳濡目染，也受益匪浅，算是大势所趋，回头看看Qunee组件，借这个趋势，可以在云计算可视化上发挥作用，最近就有客户用Qunee实现VPC配置图，并对交互做了定制，细节不便多说，本文主要介绍Qunee交互扩展方面的思路</p><p>云计算大行其道，上期开源中国的原创会就有好几位云计算专家演讲，从底层的虚拟化技术，到上层的云存储和应用API，耳濡目染，也受益匪浅，算是大势所趋，回头看看Qunee组件，借这个趋势，可以在云计算可视化上发挥作用，最近就有客户用Qunee实现VPC配置图，并对交互做了定制，细节不便多说，本文主要介绍Qunee交互扩展方面的思路</p><p><br/></p>', '2', 'uploads\\20180915\\de745e1dd252517d4557ae18e34bbf60.jpg', '原创', '1537023128', '0', '0', '1', '0');
INSERT INTO `yf_article` VALUES ('6', 'HTML5简介及HTML5的发展前景', 'HTML5', 'HTML5是最新一代的HTML标准，它不仅拥有HTML中所有的特性，而且增加了许多实用的特性，如视频、音频、画布（canvas）等。2012年12月17日，万维网联盟（W3C）', 'HTML5是最新一代的HTML标准，它不仅拥有HTML中所有的特性，而且增加了许多实用的特性，如视频、音频、画布（canvas）等。2012年12月17日，万维网联盟（W3C）', '<h2 style=\"margin: 1.71429rem 0px; padding: 0px; border: 0px; font-size: 1.28571rem; vertical-align: baseline; clear: both; line-height: 1.6; color: rgb(68, 68, 68); font-family: Helvetica, Arial, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);\">什么是HTML5</h2><p style=\"margin-top: 0px; margin-bottom: 1.71429rem; padding: 0px; border: 0px; font-size: 14px; vertical-align: baseline; line-height: 1.71429; color: rgb(68, 68, 68); font-family: Helvetica, Arial, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);\">HTML5是最新一代的HTML标准，它不仅拥有HTML中所有的特性，而且增加了许多实用的特性，如视频、音频、画布（canvas）等。2012年12月17日，万维网联盟（W3C）正式宣布凝结了大量网络工作者心血的HTML5规范已经正式定稿。根据W3C的发言稿称：“HTML5是开放的Web网络平台的奠基石。”</p><h2 style=\"margin: 1.71429rem 0px; padding: 0px; border: 0px; font-size: 1.28571rem; vertical-align: baseline; clear: both; line-height: 1.6; color: rgb(68, 68, 68); font-family: Helvetica, Arial, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);\">HTML5可以做什么</h2><p style=\"margin-top: 0px; margin-bottom: 1.71429rem; padding: 0px; border: 0px; font-size: 14px; vertical-align: baseline; line-height: 1.71429; color: rgb(68, 68, 68); font-family: Helvetica, Arial, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);\">HTML5作为HTML标准，你当然可以用它来实现之前HTML可以实现的功能，除此之外，我们还可以用HTML5做以下特别的事情：</p><h3 style=\"margin: 1.71429rem 0px; padding: 0px; border: 0px; font-size: 1.14286rem; vertical-align: baseline; clear: both; line-height: 1.84615; color: rgb(68, 68, 68); font-family: Helvetica, Arial, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);\">1、本地存储</h3><p style=\"margin-top: 0px; margin-bottom: 1.71429rem; padding: 0px; border: 0px; font-size: 14px; vertical-align: baseline; line-height: 1.71429; color: rgb(68, 68, 68); font-family: Helvetica, Arial, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);\">基于HTML5开发的网页APP拥有更短的启动时间，更快的联网速度，这些全得益于HTML5 APP Cache，以及本地存储功能。Indexed DB（html5本地存储最重要的技术之一）和API说明文档。</p><h3 style=\"margin: 1.71429rem 0px; padding: 0px; border: 0px; font-size: 1.14286rem; vertical-align: baseline; clear: both; line-height: 1.84615; color: rgb(68, 68, 68); font-family: Helvetica, Arial, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);\">2、实现多媒体更加简单</h3><p style=\"margin-top: 0px; margin-bottom: 1.71429rem; padding: 0px; border: 0px; font-size: 14px; vertical-align: baseline; line-height: 1.71429; color: rgb(68, 68, 68); font-family: Helvetica, Arial, sans-serif; white-space: normal; background-color: rgb(255, 255, 255);\">我们可以利用HTML5的&lt;video&gt;和&amp;[......]</p><p><br/></p>', '2', '', '原创', '1537023174', '0', '0', '1', '0');
INSERT INTO `yf_article` VALUES ('7', 'PHP能做什么', 'PHP', 'HP 可以生成动态页面内容  PHP 可以创建、打开、读取、写入、关闭服务器上的文件', 'HP 可以生成动态页面内容  PHP 可以创建、打开、读取、写入、关闭服务器上的文件  PHP 可以收集表单数据', '<ul style=\"list-style-type: none;\" class=\" list-paddingleft-2\"><li><p>PHP 可以生成动态页面内容<br/></p></li><li><p>PHP 可以创建、打开、读取、写入、关闭服务器上的文件<br/></p></li><li><p>PHP 可以收集表单数据<br/></p></li><li><p>PHP 可以发送和接收 cookies<br/></p></li><li><p>PHP 可以添加、删除、修改您的数据库中的数据<br/></p></li><li><p>PHP 可以限制用户访问您的网站上的一些页面<br/></p></li><li><p>PHP 可以加密数据</p></li></ul><p><br/></p>', '1', 'uploads\\20180915\\c719f3c16887442774d7cdb5ea96c280.jpg', '原创', '1537023238', '0', '0', '1', '0');
INSERT INTO `yf_article` VALUES ('8', 'PHP局部和全局作用域', 'PHP作用域', '在所有函数外部定义的变量，拥有全局作用域。除了函数外，全局变量可以被脚本中的任何部分访问，要在一个函数中访问一个全局变量，需要使用 global 关键字。  在 PHP 函数内部声明的变量是局部变量，仅能在函数内部访问：', '在所有函数外部定义的变量，拥有全局作用域。除了函数外，全局变量可以被脚本中的任何部分访问，要在一个函数中访问一个全局变量，需要使用 global 关键字。  在 PHP 函数内部声明的变量是局部变量，仅能在函数内部访问：', '<p style=\"border: 0px; margin-top: 0px; margin-bottom: 0px; padding: 0px; line-height: 2em; word-wrap: break-word; word-break: break-all; font-size: 14px; font-family: &quot;Helvetica Neue&quot;, Helvetica, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, Arial, sans-serif; color: rgb(51, 51, 51); white-space: normal; background-color: rgb(255, 255, 255);\">在所有函数外部定义的变量，拥有全局作用域。除了函数外，全局变量可以被脚本中的任何部分访问，要在一个函数中访问一个全局变量，需要使用 global 关键字。</p><p style=\"border: 0px; margin-top: 0px; margin-bottom: 0px; padding: 0px; line-height: 2em; word-wrap: break-word; word-break: break-all; font-size: 14px; font-family: &quot;Helvetica Neue&quot;, Helvetica, &quot;PingFang SC&quot;, &quot;Hiragino Sans GB&quot;, &quot;Microsoft YaHei&quot;, &quot;Noto Sans CJK SC&quot;, &quot;WenQuanYi Micro Hei&quot;, Arial, sans-serif; color: rgb(51, 51, 51); white-space: normal; background-color: rgb(255, 255, 255);\">在 PHP 函数内部声明的变量是局部变量，仅能在函数内部访问：</p><pre class=\"brush:php;toolbar:false\">&lt;?php\r\n$x=5;&nbsp;//&nbsp;全局变量\r\nfunction&nbsp;myTest(){&nbsp;\r\n&nbsp;&nbsp;&nbsp;&nbsp;$y=10;&nbsp;//&nbsp;局部变量\r\n&nbsp;&nbsp;&nbsp;&nbsp;echo&nbsp;&quot;&lt;p&gt;测试变量在函数内部:&lt;p&gt;&quot;;&nbsp;&nbsp;&nbsp;&nbsp;\r\n&nbsp;&nbsp;&nbsp;&nbsp;echo&nbsp;&quot;变量&nbsp;x&nbsp;为:&nbsp;$x&quot;;&nbsp;&nbsp;&nbsp;&nbsp;\r\n&nbsp;&nbsp;&nbsp;&nbsp;echo&nbsp;&quot;&lt;br&gt;&quot;;&nbsp;&nbsp;&nbsp;\r\n&nbsp;&nbsp;&nbsp;&nbsp;echo&nbsp;&quot;变量&nbsp;y&nbsp;为:&nbsp;$y&quot;;\r\n&nbsp;&nbsp;&nbsp;&nbsp;}\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;myTest();\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;echo&nbsp;&quot;&lt;p&gt;测试变量在函数外部:&lt;p&gt;&quot;;\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;echo&nbsp;&quot;变量&nbsp;x&nbsp;为:&nbsp;$x&quot;;\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;echo&nbsp;&quot;&lt;br&gt;&quot;;\r\n&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;echo&nbsp;&quot;变量&nbsp;y&nbsp;为:&nbsp;$y&quot;;\r\n?&gt;</pre><p><br/></p>', '1', 'uploads\\20180915\\e39974eaac7b837ded99831090f22576.jpg', '原创', '1537023415', '6', '0', '1', '0');
INSERT INTO `yf_article` VALUES ('9', ' ThinkPHP5.1.24版本发布——命令行增强，增加查看路由列表指令', 'ThinkPHP5', '该版本主要增加了命令行的表格输出功能，并增加了查看路由定义以及快速生成命令行类的指令，以及修正了社区的一些反馈问题。支持上一个版本无缝升级！', '该版本主要增加了命令行的表格输出功能，并增加了查看路由定义以及快速生成命令行类的指令，以及修正了社区的一些反馈问题。支持上一个版本无缝升级！', '<p><span style=\"color: rgb(50, 50, 50); font-family: &quot;Century Gothic&quot;, &quot;Microsoft yahei&quot;; background-color: rgb(255, 255, 255);\">该版本主要增加了命令行的表格输出功能，并增加了查看路由定义以及快速生成命令行类的指令，以及修正了社区的一些反馈问题。支持上一个版本无缝升级！</span></p><h2 style=\"margin: 0px; padding: 0px; color: rgb(50, 50, 50); font-family: &quot;Century Gothic&quot;, &quot;Microsoft yahei&quot;; white-space: normal; background-color: rgb(255, 255, 255);\">【主要更新日志】</h2><p><span style=\"color: rgb(50, 50, 50); font-family: &quot;Century Gothic&quot;, &quot;Microsoft yahei&quot;; background-color: rgb(255, 255, 255);\">* 修正`Request`类的`file`方法</span><br/><span style=\"color: rgb(50, 50, 50); font-family: &quot;Century Gothic&quot;, &quot;Microsoft yahei&quot;; background-color: rgb(255, 255, 255);\">* 修正路由的`cache`方法</span><br/><span style=\"color: rgb(50, 50, 50); font-family: &quot;Century Gothic&quot;, &quot;Microsoft yahei&quot;; background-color: rgb(255, 255, 255);\">* 修正路由缓存的一处问题</span><br/><span style=\"color: rgb(50, 50, 50); font-family: &quot;Century Gothic&quot;, &quot;Microsoft yahei&quot;; background-color: rgb(255, 255, 255);\">* 改进上传文件获取的异常处理</span><br/><span style=\"color: rgb(50, 50, 50); font-family: &quot;Century Gothic&quot;, &quot;Microsoft yahei&quot;; background-color: rgb(255, 255, 255);\">* 改进`fetchCollection`方法支持传入数据集类名</span><br/><span style=\"color: rgb(50, 50, 50); font-family: &quot;Century Gothic&quot;, &quot;Microsoft yahei&quot;; background-color: rgb(255, 255, 255);\">* 修正多级控制器的注解路由生成</span><br/><span style=\"color: rgb(50, 50, 50); font-family: &quot;Century Gothic&quot;, &quot;Microsoft yahei&quot;; background-color: rgb(255, 255, 255);\">* 改进`Middleware`类`clear`方法</span><br/><span style=\"color: rgb(50, 50, 50); font-family: &quot;Century Gothic&quot;, &quot;Microsoft yahei&quot;; background-color: rgb(255, 255, 255);\">* 增加`route:list`指令用于</span><a href=\"https://www.kancloud.cn/manual/thinkphp5_1/752690\" target=\"_blank\" style=\"color: rgb(114, 185, 57); text-decoration-line: none; font-family: &quot;Century Gothic&quot;, &quot;Microsoft yahei&quot;; white-space: normal; background-color: rgb(255, 255, 255);\">查看定义的路由</a><span style=\"color: rgb(50, 50, 50); font-family: &quot;Century Gothic&quot;, &quot;Microsoft yahei&quot;; background-color: rgb(255, 255, 255);\">&nbsp;并支持排序</span><br/><span style=\"color: rgb(50, 50, 50); font-family: &quot;Century Gothic&quot;, &quot;Microsoft yahei&quot;; background-color: rgb(255, 255, 255);\">* 命令行增加`Table`输出类</span><br/><span style=\"color: rgb(50, 50, 50); font-family: &quot;Century Gothic&quot;, &quot;Microsoft yahei&quot;; background-color: rgb(255, 255, 255);\">* `Command`类增加`table`方法用于输出表格</span><br/><span style=\"color: rgb(50, 50, 50); font-family: &quot;Century Gothic&quot;, &quot;Microsoft yahei&quot;; background-color: rgb(255, 255, 255);\">* 改进搜索器查询方法支持别名定义</span><br/><span style=\"color: rgb(50, 50, 50); font-family: &quot;Century Gothic&quot;, &quot;Microsoft yahei&quot;; background-color: rgb(255, 255, 255);\">* 命令行配置增加`auto_path`参数用于定义自动载入的命令类路径</span><br/><span style=\"color: rgb(50, 50, 50); font-family: &quot;Century Gothic&quot;, &quot;Microsoft yahei&quot;; background-color: rgb(255, 255, 255);\">* 增加`make:command`指令用于</span><a href=\"https://www.kancloud.cn/manual/thinkphp5_1/354146\" target=\"_blank\" style=\"color: rgb(114, 185, 57); text-decoration-line: none; font-family: &quot;Century Gothic&quot;, &quot;Microsoft yahei&quot;; white-space: normal; background-color: rgb(255, 255, 255);\">快速生成指令</a><br/><span style=\"color: rgb(50, 50, 50); font-family: &quot;Century Gothic&quot;, &quot;Microsoft yahei&quot;; background-color: rgb(255, 255, 255);\">* 改进`make:controller`指令对操作方法后缀的支持</span><br/><span style=\"color: rgb(50, 50, 50); font-family: &quot;Century Gothic&quot;, &quot;Microsoft yahei&quot;; background-color: rgb(255, 255, 255);\">* 改进命令行的定义文件支持索引数组 用于指令对象的惰性加载</span><br/><span style=\"color: rgb(50, 50, 50); font-family: &quot;Century Gothic&quot;, &quot;Microsoft yahei&quot;; background-color: rgb(255, 255, 255);\">* 改进`value`和`column`方法对后续查询结果的影响</span><br/><span style=\"color: rgb(50, 50, 50); font-family: &quot;Century Gothic&quot;, &quot;Microsoft yahei&quot;; background-color: rgb(255, 255, 255);\">* 改进`RuleName`类的`setRule`方法</span></p><p><br/></p>', '3', 'uploads\\20180915\\708d523e55cd1dc9b440ad5c7c8b2f50.jpg', '原创', '1537023486', '15', '0', '1', '0');
INSERT INTO `yf_article` VALUES ('10', '测试博文', '', '', '', '<p>5765757</p>', '25', 'uploads\\20180928\\029cb42ce853ca20d4b532d053b22771.jpg', '原创', '1538066871', '34', '0', '1', '0');

-- ----------------------------
-- Table structure for yf_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `yf_auth_group`;
CREATE TABLE `yf_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `rules` char(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yf_auth_group
-- ----------------------------
INSERT INTO `yf_auth_group` VALUES ('1', '超级管理员', '1', '1,2,4,3,5,6,21,11,12,13,14,15,16,20,17');
INSERT INTO `yf_auth_group` VALUES ('8', '普通管理员', '1', '3,5,6,21,11,12,16,20,22,23,24,25,26,27,28,29,30');
INSERT INTO `yf_auth_group` VALUES ('9', '123', '1', '');

-- ----------------------------
-- Table structure for yf_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `yf_auth_group_access`;
CREATE TABLE `yf_auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yf_auth_group_access
-- ----------------------------
INSERT INTO `yf_auth_group_access` VALUES ('9', '1');
INSERT INTO `yf_auth_group_access` VALUES ('20', '8');

-- ----------------------------
-- Table structure for yf_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `yf_auth_rule`;
CREATE TABLE `yf_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(80) NOT NULL DEFAULT '',
  `title` char(20) NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `condition` char(100) NOT NULL DEFAULT '',
  `pid` int(11) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yf_auth_rule
-- ----------------------------
INSERT INTO `yf_auth_rule` VALUES ('1', 'sys', '系统设置', '1', '1', '', '0');
INSERT INTO `yf_auth_rule` VALUES ('2', 'config/index', '基本设置', '1', '1', '', '1');
INSERT INTO `yf_auth_rule` VALUES ('3', 'addon', '插件管理', '1', '1', '', '0');
INSERT INTO `yf_auth_rule` VALUES ('5', 'Addons/install', '安装插件', '1', '1', '', '3');
INSERT INTO `yf_auth_rule` VALUES ('6', 'Addons/uninstall', '卸载插件', '1', '1', '', '3');
INSERT INTO `yf_auth_rule` VALUES ('11', 'manager', '管理员管理', '1', '1', '', '0');
INSERT INTO `yf_auth_rule` VALUES ('12', 'Manager/index', '管理员列表', '1', '1', '', '11');
INSERT INTO `yf_auth_rule` VALUES ('13', 'Manager/edit', '编辑管理员', '1', '1', '', '11');
INSERT INTO `yf_auth_rule` VALUES ('14', 'Manager/add', '添加管理员', '1', '1', '', '11');
INSERT INTO `yf_auth_rule` VALUES ('15', 'AuthGroup/index', '角色管理', '1', '1', '', '11');
INSERT INTO `yf_auth_rule` VALUES ('16', 'Rule/index', '权限列表', '1', '1', '', '35');
INSERT INTO `yf_auth_rule` VALUES ('17', 'Rule/add', '添加权限', '1', '1', '', '35');
INSERT INTO `yf_auth_rule` VALUES ('20', 'Rule/setstatus', '设置状态', '1', '1', '', '16');
INSERT INTO `yf_auth_rule` VALUES ('21', 'addons/index', '插件列表', '1', '1', '', '3');
INSERT INTO `yf_auth_rule` VALUES ('22', 'Database', '数据库管理', '1', '1', '', '0');
INSERT INTO `yf_auth_rule` VALUES ('23', 'Database/index', '数据库列表', '1', '1', '', '22');
INSERT INTO `yf_auth_rule` VALUES ('24', 'Database/repair', '单表修复', '1', '1', '', '22');
INSERT INTO `yf_auth_rule` VALUES ('25', 'Database/optimize', '单表优化', '1', '1', '', '22');
INSERT INTO `yf_auth_rule` VALUES ('26', 'Database/repairAll', '批量修复', '1', '1', '', '22');
INSERT INTO `yf_auth_rule` VALUES ('27', 'Database/backuplst', '备份列表', '1', '1', '', '22');
INSERT INTO `yf_auth_rule` VALUES ('28', 'Database/dbbackup', '数据库备份', '1', '1', '', '22');
INSERT INTO `yf_auth_rule` VALUES ('29', 'Database/del', '备份删除', '1', '1', '', '22');
INSERT INTO `yf_auth_rule` VALUES ('30', 'Database/restore', '备份还原', '1', '1', '', '22');
INSERT INTO `yf_auth_rule` VALUES ('31', 'Logs', '行为日志', '1', '1', '', '0');
INSERT INTO `yf_auth_rule` VALUES ('32', 'Logs/index', '日志列表', '1', '1', '', '31');
INSERT INTO `yf_auth_rule` VALUES ('33', 'Logs/del', '删除日志', '1', '1', '', '31');
INSERT INTO `yf_auth_rule` VALUES ('34', 'Manager/save', '保存管理员', '1', '1', '', '11');
INSERT INTO `yf_auth_rule` VALUES ('35', 'Rule', '权限管理', '1', '1', '', '0');
INSERT INTO `yf_auth_rule` VALUES ('36', 'Rule/edit', '编辑权限', '1', '1', '', '35');
INSERT INTO `yf_auth_rule` VALUES ('37', 'Rule/save', '保存权限', '1', '1', '', '35');

-- ----------------------------
-- Table structure for yf_blogcate
-- ----------------------------
DROP TABLE IF EXISTS `yf_blogcate`;
CREATE TABLE `yf_blogcate` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL COMMENT '分类名称',
  `pic` varchar(150) DEFAULT NULL COMMENT '分类图片',
  `sort` int(11) DEFAULT '100' COMMENT '排序',
  `isshow` int(11) DEFAULT '0' COMMENT '是否栏目显示（0：不显示,1:显示）',
  `uid` int(11) NOT NULL COMMENT '所属用户',
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COMMENT='分类表';

-- ----------------------------
-- Records of yf_blogcate
-- ----------------------------
INSERT INTO `yf_blogcate` VALUES ('1', 'PHP基础课程', '', '111', '0', '1');
INSERT INTO `yf_blogcate` VALUES ('2', 'HTML5', 'uploads\\20180618\\91d3f19fac406bb64ae5a03be92f2cb4.jpg', '200', '1', '1');
INSERT INTO `yf_blogcate` VALUES ('3', 'ThinkPHP5.0', '', '100', '0', '1');
INSERT INTO `yf_blogcate` VALUES ('25', '测试分类', '', '100', '1', '2');

-- ----------------------------
-- Table structure for yf_logs
-- ----------------------------
DROP TABLE IF EXISTS `yf_logs`;
CREATE TABLE `yf_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` char(30) NOT NULL COMMENT '操作节点-控制器和方法',
  `operator` int(11) NOT NULL DEFAULT '0' COMMENT '操作员，管理员id',
  `description` char(60) NOT NULL COMMENT '描述',
  `operate_time` int(10) NOT NULL COMMENT '操作时间',
  `operate_ip` varchar(20) NOT NULL COMMENT '操作IP',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=141 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yf_logs
-- ----------------------------
INSERT INTO `yf_logs` VALUES ('1', 'Logs/index', '9', '日志列表', '1541567597', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('2', 'Logs/index', '9', '日志列表', '1541568366', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('3', 'Index/logout', '0', '退出', '1541568368', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('4', 'Index/index', '0', '访问登录页面', '1541568368', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('5', 'Index/dologin', '9', '正在登录', '1541568374', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('6', 'Index/index', '9', '登录成功', '1541568378', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('7', 'Index/welcome', '9', '欢迎页面', '1541568378', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('8', 'Logs/index', '9', '日志列表', '1541568380', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('9', 'Index/logout', '0', '退出', '1541568435', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('10', 'Index/index', '0', '访问登录页面', '1541568436', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('11', 'Index/dologin', '20', '登录成功', '1541568505', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('12', 'Index/index', '20', '登录成功', '1541568508', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('13', 'Index/welcome', '20', '欢迎页面', '1541568509', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('14', 'Logs/index', '20', '日志列表', '1541568512', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('15', 'Index/welcome', '20', '欢迎页面', '1541568515', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('16', 'Index/logout', '0', '退出', '1541568516', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('17', 'Index/index', '0', '访问登录页面', '1541568516', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('18', 'Index/dologin', '0', '账号不存在', '1541568528', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('19', 'Index/dologin', '0', '账号不存在', '1541568530', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('20', 'Index/dologin', '0', '密码不正确', '1541568532', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('21', 'Index/dologin', '9', '登录成功', '1541568537', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('22', 'Index/index', '9', '登录成功', '1541568540', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('23', 'Index/welcome', '9', '欢迎页面', '1541568541', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('24', 'Logs/index', '9', '日志列表', '1541568543', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('25', 'Logs/index', '9', '日志列表', '1541568609', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('26', 'Index/logout', '0', '退出', '1541568641', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('27', 'Index/index', '0', '访问登录页面', '1541568641', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('28', 'Index/dologin', '0', '登录失败', '1541568649', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('29', 'Index/dologin', '9', '登录成功', '1541568652', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('30', 'Index/index', '9', '登录成功', '1541568656', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('31', 'Index/welcome', '9', '欢迎页面', '1541568656', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('32', 'Logs/index', '9', '日志列表', '1541568658', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('33', 'Logs/index', '9', '日志列表', '1541569805', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('34', 'Logs/index', '9', '日志列表', '1541569807', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('35', 'Database/index', '9', '数据库列表', '1541569809', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('36', 'Manager/index', '9', '管理员列表', '1541570026', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('37', 'Addons/index', '9', '插件列表', '1541570028', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('38', 'Database/index', '9', '数据库列表', '1541570196', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('39', 'Index/index', '9', '登录成功', '1541570198', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('40', 'Index/welcome', '9', '欢迎页面', '1541570198', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('41', 'Database/index', '9', '数据库列表', '1541570200', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('42', 'Database/backuplst', '9', '备份列表', '1541570300', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('43', 'Database/backuplst', '9', '备份列表', '1541570362', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('44', 'Database/index', '9', '数据库列表', '1541571000', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('45', 'Database/backuplst', '9', '备份列表', '1541571001', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('46', 'Database/index', '9', '数据库列表', '1541571447', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('47', 'Database/backuplst', '9', '备份列表', '1541571450', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('48', 'Database/index', '9', '数据库列表', '1541571470', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('49', 'Database/backuplst', '9', '备份列表', '1541571471', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('50', 'Database/backuplst', '9', '备份列表', '1541571517', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('51', 'Index/index', '9', '登录成功', '1541571561', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('52', 'Index/welcome', '9', '欢迎页面', '1541571561', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('53', 'Database/index', '9', '数据库列表', '1541571564', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('54', 'Database/backuplst', '9', '备份列表', '1541571567', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('55', 'Database/backuplst', '9', '备份列表', '1541571588', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('56', 'Database/backuplst', '9', '备份列表', '1541571621', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('57', 'Database/backuplst', '9', '备份列表', '1541571707', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('58', 'Database/index', '9', '数据库列表', '1541571797', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('59', 'Database/backuplst', '9', '备份列表', '1541571806', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('60', 'Database/backuplst', '9', '备份列表', '1541571854', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('61', 'Database/download', '9', '未知', '1541571857', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('62', 'Database/download', '9', '未知', '1541571868', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('63', 'Database/download', '9', '未知', '1541572040', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('64', 'Database/backuplst', '9', '备份列表', '1541572131', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('65', 'Database/backuplst', '9', '备份列表', '1541572206', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('66', 'Database/index', '9', '数据库列表', '1541572534', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('67', 'Database/backuplst', '9', '备份列表', '1541572536', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('68', 'Database/index', '9', '数据库列表', '1541572572', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('69', 'Database/backuplst', '9', '备份列表', '1541572573', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('70', 'Database/backuplst', '9', '备份列表', '1541572752', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('71', 'Index/index', '9', '登录成功', '1541572764', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('72', 'Index/welcome', '9', '欢迎页面', '1541572765', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('73', 'Database/index', '9', '数据库列表', '1541572767', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('74', 'Database/backuplst', '9', '备份列表', '1541572770', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('75', 'Database/backuplst', '9', '备份列表', '1541572838', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('76', 'Database/download', '9', '未知', '1541572840', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('77', 'Database/backuplst', '9', '备份列表', '1541572866', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('78', 'Database/download', '9', '未知', '1541572869', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('79', 'Database/index', '9', '数据库列表', '1541574133', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('80', 'Index/index', '9', '登录成功', '1541574134', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('81', 'Index/welcome', '9', '欢迎页面', '1541574135', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('82', 'Database/index', '9', '数据库列表', '1541574137', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('83', 'Database/backuplst', '9', '备份列表', '1541574139', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('84', 'Database/dbbackup', '9', '数据库备份', '1541574141', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('85', 'Database/backuplst', '9', '备份列表', '1541574143', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('86', 'Database/index', '9', '数据库列表', '1541574156', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('87', 'Database/backuplst', '9', '备份列表', '1541574158', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('88', 'Database/del', '9', '备份删除', '1541574163', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('89', 'Database/backuplst', '9', '备份列表', '1541574165', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('90', 'Database/dbbackup', '9', '数据库备份', '1541574167', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('91', 'Database/backuplst', '9', '备份列表', '1541574168', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('92', 'Database/del', '9', '备份删除', '1541574172', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('93', 'Database/backuplst', '9', '备份列表', '1541574174', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('94', 'Database/dbbackup', '9', '数据库备份', '1541574204', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('95', 'Database/backuplst', '9', '备份列表', '1541574206', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('96', 'Database/index', '9', '数据库列表', '1541574288', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('97', 'Database/backuplst', '9', '备份列表', '1541574289', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('98', 'Database/dbbackup', '9', '数据库备份', '1541574293', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('99', 'Database/backuplst', '9', '备份列表', '1541574294', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('100', 'Database/index', '9', '数据库列表', '1541574326', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('101', 'Database/backuplst', '9', '备份列表', '1541574327', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('102', 'Database/index', '9', '数据库列表', '1541574350', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('103', 'Database/backuplst', '9', '备份列表', '1541574351', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('104', 'Database/download', '9', '未知', '1541574354', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('105', 'Database/index', '9', '数据库列表', '1541574618', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('106', 'Index/index', '9', '登录成功', '1541575254', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('107', 'Index/welcome', '9', '欢迎页面', '1541575254', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('108', 'Index/index', '9', '登录成功', '1541575302', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('109', 'Index/welcome', '9', '欢迎页面', '1541575302', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('110', 'Database/index', '9', '数据库列表', '1541575404', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('111', 'Database/backuplst', '9', '备份列表', '1541575420', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('112', 'Index/index', '9', '登录成功', '1541575503', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('113', 'Index/index', '9', '登录成功', '1541575504', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('114', 'Index/welcome', '9', '欢迎页面', '1541575505', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('115', 'Index/index', '9', '登录成功', '1541575546', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('116', 'Index/welcome', '9', '欢迎页面', '1541575546', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('117', 'Database/index', '9', '数据库列表', '1541575824', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('118', 'Database/backuplst', '9', '备份列表', '1541575827', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('119', 'Database/index', '9', '数据库列表', '1541576253', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('120', 'Database/backuplst', '9', '备份列表', '1541576256', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('121', 'Database/restore', '9', '备份还原', '1541576293', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('122', 'Database/backuplst', '9', '备份列表', '1541576295', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('123', 'Database/del', '9', '备份删除', '1541576296', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('124', 'Database/backuplst', '9', '备份列表', '1541576298', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('125', 'Database/index', '9', '数据库列表', '1541578248', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('126', 'Database/index', '9', '数据库列表', '1541578249', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('127', 'Database/index', '9', '数据库列表', '1541578249', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('128', 'Database/index', '9', '数据库列表', '1541578249', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('129', 'Database/index', '9', '数据库列表', '1541578249', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('130', 'Database/index', '9', '数据库列表', '1541578250', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('131', 'Database/index', '9', '数据库列表', '1541578260', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('132', 'Database/index', '9', '数据库列表', '1541580783', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('133', 'Database/index', '9', '数据库列表', '1541580784', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('134', 'Database/index', '9', '数据库列表', '1541582464', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('135', 'Database/backuplst', '9', '备份列表', '1541582464', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('136', 'Database/restore', '9', '备份还原', '1541582555', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('137', 'Database/backuplst', '9', '备份列表', '1541582557', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('138', 'Database/index', '9', '数据库列表', '1541582557', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('139', 'Manager/index', '9', '管理员列表', '1541582559', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('140', 'Logs/index', '9', '日志列表', '1541586171', '127.0.0.1');

-- ----------------------------
-- Table structure for yf_manager
-- ----------------------------
DROP TABLE IF EXISTS `yf_manager`;
CREATE TABLE `yf_manager` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(50) NOT NULL COMMENT '账号',
  `passwd` varchar(32) NOT NULL COMMENT '密码',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '管理员状态',
  `create_time` int(10) NOT NULL COMMENT '创建时间',
  `update_time` int(10) NOT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yf_manager
-- ----------------------------
INSERT INTO `yf_manager` VALUES ('9', 'admin', '96e79218965eb72c92a549dd5a330112', '1', '0', '1541086917');
INSERT INTO `yf_manager` VALUES ('20', 'admin1', '96e79218965eb72c92a549dd5a330112', '1', '1541086852', '1541086852');

-- ----------------------------
-- Table structure for yf_pics
-- ----------------------------
DROP TABLE IF EXISTS `yf_pics`;
CREATE TABLE `yf_pics` (
  `aid` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL COMMENT '标题',
  `keyword` varchar(100) DEFAULT NULL COMMENT '关键字',
  `desc` varchar(150) DEFAULT NULL COMMENT '描述',
  `remark` varchar(150) DEFAULT NULL COMMENT '摘要',
  `content` text COMMENT '内容',
  `cid` int(11) NOT NULL COMMENT '分类ID',
  `pic` varchar(150) DEFAULT NULL COMMENT '图片',
  `source` varchar(45) DEFAULT NULL COMMENT '来源',
  `addtime` int(10) DEFAULT NULL COMMENT '发布时间',
  `views` int(11) DEFAULT '0' COMMENT '浏览次数',
  `ischeck` tinyint(1) DEFAULT '0' COMMENT '0：审核不通过，1：审核通过',
  `isnominate` tinyint(1) DEFAULT '0' COMMENT '0：不推荐 ，1：推荐',
  `istop` tinyint(1) DEFAULT '0' COMMENT '0：不置项，1：置顶',
  PRIMARY KEY (`aid`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='文章表';

-- ----------------------------
-- Records of yf_pics
-- ----------------------------
INSERT INTO `yf_pics` VALUES ('2', '风景1', '', '', '', '<p><img src=\"/ueditor/php/upload/image/20180724/1532442944634116.jpg\" title=\"1532442944634116.jpg\" alt=\"1.jpg\"/></p>', '2', 'uploads\\20180724\\d29d07ac7f71acc1e6d7f4ee61ca339b.jpg', '原创', '1532442946', '0', '0', '1', '0');
INSERT INTO `yf_pics` VALUES ('3', '风景2', '', '', '', '<p><img src=\"/ueditor/php/upload/image/20180724/1532442977909433.jpg\" title=\"1532442977909433.jpg\" alt=\"2.jpg\"/></p>', '2', 'uploads\\20180724\\465cf55f91ed9e202e2909997713d132.jpg', '原创', '1532442980', '0', '0', '1', '0');
INSERT INTO `yf_pics` VALUES ('4', '风景3', '', '', '', '<p><img src=\"/ueditor/php/upload/image/20180724/1532443018104360.jpg\" title=\"1532443018104360.jpg\" alt=\"3.jpg\"/></p>', '2', 'uploads\\20180724\\343d7854ecb39a06b8f39f6ca429c2e9.jpg', '原创', '1532443021', '0', '0', '1', '0');
INSERT INTO `yf_pics` VALUES ('5', '风景4', '', '', '', '<p><img src=\"/ueditor/php/upload/image/20180724/1532443036105772.jpg\" title=\"1532443036105772.jpg\" alt=\"4.jpg\"/></p>', '2', 'uploads\\20180724\\18d7e7c4e22bf34dc3ac3e5b7282621a.jpg', '原创', '1532443038', '0', '0', '1', '0');
INSERT INTO `yf_pics` VALUES ('6', '风景5', '', '', '', '<p><img src=\"/ueditor/php/upload/image/20180724/1532443055469907.jpg\" title=\"1532443055469907.jpg\" alt=\"5.jpg\"/></p>', '2', 'uploads\\20180724\\a43c1a3beaa014e06acb967d01a83c6a.jpg', '原创', '1532443058', '0', '0', '1', '0');
INSERT INTO `yf_pics` VALUES ('7', '风景6', '', '', '', '<p><img src=\"/ueditor/php/upload/image/20180724/1532443079989386.jpg\" title=\"1532443079989386.jpg\" alt=\"6.jpg\"/></p>', '2', 'uploads\\20180724\\5d1e5819dc0bd40530a8cb87f58ce928.jpg', '原创', '1532443082', '0', '0', '1', '0');
INSERT INTO `yf_pics` VALUES ('8', '风景7', '', '', '', '<p><img src=\"/ueditor/php/upload/image/20180724/1532443100411986.jpg\" title=\"1532443100411986.jpg\" alt=\"7.jpg\"/></p>', '2', 'uploads\\20180724\\5900b58f11c00fb3ba79633f5cfaf739.jpg', '原创', '1532443102', '0', '0', '1', '0');
INSERT INTO `yf_pics` VALUES ('9', '风景8', '', '', '', '<p><img src=\"/ueditor/php/upload/image/20180724/1532443117807792.jpg\" title=\"1532443117807792.jpg\" alt=\"8.jpg\"/></p>', '2', 'uploads\\20180724\\e9946ff1897008d4d263fb09b8dc0f09.jpg', '原创', '1532443118', '0', '0', '1', '0');
INSERT INTO `yf_pics` VALUES ('10', '风景9', '', '', '', '<p><img src=\"/ueditor/php/upload/image/20180724/1532443133133484.jpg\" title=\"1532443133133484.jpg\" alt=\"9.jpg\"/></p>', '2', 'uploads\\20180724\\37419d243a670795e2459e37f59c26c2.jpg', '原创', '1532443135', '0', '0', '1', '0');
INSERT INTO `yf_pics` VALUES ('11', '风景10', '', '', '', '<p><img src=\"/ueditor/php/upload/image/20180724/1532443151137211.jpg\" title=\"1532443151137211.jpg\" alt=\"10.jpg\"/></p>', '2', 'uploads\\20180724\\365dc6b60925d0bff80852926a656d51.jpg', '原创', '1532443152', '0', '0', '1', '0');
INSERT INTO `yf_pics` VALUES ('12', '人物1', '', '', '', '<p><img src=\"/ueditor/php/upload/image/20180724/1532443176141058.jpg\" title=\"1532443176141058.jpg\" alt=\"1.jpg\"/></p>', '3', 'uploads\\20180724\\a28331def4fc92fe5b04f0d52f6ff3b2.jpg', '原创', '1532443178', '0', '0', '1', '0');
INSERT INTO `yf_pics` VALUES ('13', '人物2', '', '', '', '<p><img src=\"/ueditor/php/upload/image/20180724/1532443187979188.jpg\" title=\"1532443187979188.jpg\" alt=\"2.jpg\"/></p>', '3', 'uploads\\20180724\\1120159e011976f30d89bc5db9a245b8.jpg', '原创', '1532443189', '0', '0', '1', '0');
INSERT INTO `yf_pics` VALUES ('14', '人物3', '', '', '', '<p><img src=\"/ueditor/php/upload/image/20180724/1532443201295198.jpg\" title=\"1532443201295198.jpg\" alt=\"3.jpg\"/></p>', '3', 'uploads\\20180724\\5b45f43d03ceb478ccb75e7d3ba5d8ee.jpg', '原创', '1532443203', '0', '0', '1', '0');
INSERT INTO `yf_pics` VALUES ('15', '人物4', '', '', '', '<p><img src=\"/ueditor/php/upload/image/20180724/1532443215737908.jpg\" title=\"1532443215737908.jpg\" alt=\"4.jpg\"/></p>', '3', 'uploads\\20180724\\b8e401beb968216c0a00dbe224d589ba.jpg', '原创', '1532443216', '0', '0', '1', '0');
INSERT INTO `yf_pics` VALUES ('16', '人物5', '', '', '', '<p><img src=\"/ueditor/php/upload/image/20180724/1532443229550204.jpg\" title=\"1532443229550204.jpg\" alt=\"5.jpg\"/></p>', '3', 'uploads\\20180724\\3ea942422e70f336a749c33c3bdb65e1.jpg', '原创', '1532443231', '0', '0', '1', '0');
INSERT INTO `yf_pics` VALUES ('17', '人物6', '', '', '', '<p><img src=\"/ueditor/php/upload/image/20180724/1532443244439010.jpg\" title=\"1532443244439010.jpg\" alt=\"6.jpg\"/></p>', '3', 'uploads\\20180724\\3286977ec261525a37aaa00f1ddf60fb.jpg', '原创', '1532443247', '0', '0', '1', '0');
INSERT INTO `yf_pics` VALUES ('18', '人物74', '', '', '', '<p><img src=\"/ueditor/php/upload/image/20180724/1532443257272477.jpg\" title=\"1532443257272477.jpg\" alt=\"7.jpg\"/></p>', '3', 'uploads\\20180724\\49c4cece1bc4f84a50bfd4b0b8a3829d.jpg', '原创', '1532443259', '0', '0', '1', '0');
INSERT INTO `yf_pics` VALUES ('19', '人物83', '', '', '', '<p><img src=\"/ueditor/php/upload/image/20180724/1532443276839033.jpg\" title=\"1532443276839033.jpg\" alt=\"8.jpg\"/></p>', '3', 'uploads\\20180724\\c51cbbaecdadd73dbae037adc066b385.jpg', '原创', '1532443281', '0', '0', '1', '0');
INSERT INTO `yf_pics` VALUES ('20', '人物93', '', '', '', '<p><img src=\"/ueditor/php/upload/image/20180724/1532443294426875.jpg\" title=\"1532443294426875.jpg\" alt=\"9.jpg\"/></p>', '3', 'uploads\\20180724\\e95117f45f3d4eddc9ae8a28224da7df.jpg', '摘录', '1532443296', '0', '0', '1', '0');

-- ----------------------------
-- Table structure for yf_picscate
-- ----------------------------
DROP TABLE IF EXISTS `yf_picscate`;
CREATE TABLE `yf_picscate` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL COMMENT '分类名称',
  `pic` varchar(150) DEFAULT NULL COMMENT '分类图片',
  `sort` int(11) DEFAULT '100' COMMENT '排序',
  `isshow` int(11) DEFAULT '0' COMMENT '是否栏目显示（0：不显示,1:显示）',
  `uid` int(11) NOT NULL COMMENT '所属用户',
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='分类表';

-- ----------------------------
-- Records of yf_picscate
-- ----------------------------
INSERT INTO `yf_picscate` VALUES ('2', '风景', '', '100', '1', '1');
INSERT INTO `yf_picscate` VALUES ('3', '人物', '', '100', '1', '1');
INSERT INTO `yf_picscate` VALUES ('4', '动物', '', '100', '1', '1');
INSERT INTO `yf_picscate` VALUES ('5', '设计', '', '100', '1', '1');
INSERT INTO `yf_picscate` VALUES ('6', '艺术品', '', '100', '1', '1');
INSERT INTO `yf_picscate` VALUES ('7', '文物', '', '100', '1', '1');

-- ----------------------------
-- Table structure for yf_user
-- ----------------------------
DROP TABLE IF EXISTS `yf_user`;
CREATE TABLE `yf_user` (
  `uid` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户id',
  `account` varchar(45) NOT NULL COMMENT '账号',
  `passwd` varchar(32) DEFAULT NULL COMMENT '密码',
  `email` varchar(45) NOT NULL DEFAULT '' COMMENT '邮箱',
  `regtime` int(10) DEFAULT NULL COMMENT '注册时间',
  `salt` varchar(32) DEFAULT NULL COMMENT '辅助加密',
  `token` char(32) DEFAULT NULL COMMENT '激活码',
  `token_exptime` int(10) DEFAULT '0' COMMENT '激活码有效期',
  `isactive` tinyint(1) DEFAULT '0' COMMENT '激活状态1：激活，0未激活',
  `logo` varchar(150) DEFAULT NULL COMMENT '博客logo',
  `blogtitle` varchar(80) DEFAULT NULL COMMENT '博客名称',
  `blogname` varchar(150) DEFAULT NULL COMMENT '副标题',
  `host` char(10) DEFAULT NULL COMMENT '域名',
  `info` varchar(200) DEFAULT NULL COMMENT '博客介绍',
  `content` text COMMENT '关于我们',
  `face` varchar(150) DEFAULT NULL COMMENT '头像',
  `nick` varchar(35) DEFAULT NULL COMMENT '昵称',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `account_UNIQUE` (`account`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yf_user
-- ----------------------------
INSERT INTO `yf_user` VALUES ('1', '111', '34a3ed400da3afacac34c6180bab4f00', '3301423@qq.com', '1522740917', '069208139ea9197a2f9b8d86baa2948c', 'b64d3556545a6a3b044a1af4183e0e59', '1522827317', '1', '', '1', '', '', '', '', '', '');
INSERT INTO `yf_user` VALUES ('2', '222', 'b691d3d86c5b7ba065e98b7bcceb5c14', '5766171091@qq.com', '1523170001', 'fbf14f4ed2077a29e97b6834d465ee30', '1f5b095658bf2ce54f343ac6c2679d8f', '1523256401', '1', '', '', '', '', '', '', '', '123');
INSERT INTO `yf_user` VALUES ('6', 'yifeng', '08dfa1f37e7a8d8389dce62e094b8bf9', '576617109@qq.com', '1523348778', 'cc5c5349892d307ce54797a2b49b23c9', 'debc16e53f7dd9296f8fc6efcd99e741', '1523435178', '1', 'uploads\\20180411\\b557c78ba2b68823f2530a63d1e58157.png', '', '1231231', '', '1', '1', 'uploads\\20180411\\42db51a8efadae1ce2219c5871bbc79d.png', '121212');

-- ----------------------------
-- Table structure for yf_users
-- ----------------------------
DROP TABLE IF EXISTS `yf_users`;
CREATE TABLE `yf_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userkey` varchar(32) DEFAULT NULL COMMENT '用户惟一标识',
  `nickname` varchar(50) DEFAULT NULL COMMENT '昵称',
  `faceimg` varchar(200) DEFAULT NULL COMMENT '三方头像',
  `uid` int(11) DEFAULT NULL COMMENT '平台账号id',
  `type` tinyint(1) DEFAULT '0' COMMENT '平台类型：1：qq，2：微信，3：微博',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='第三方用户';

-- ----------------------------
-- Records of yf_users
-- ----------------------------
INSERT INTO `yf_users` VALUES ('1', '87E484F084D60C211A49BABDCE564AB6', '东云奇侠', 'E:\\phpStudy\\WWW\\blog\\public/face/2bb47d7a618cf12a6b1e6cabd0c39eb2.jpg', '1', '1');
INSERT INTO `yf_users` VALUES ('3', '085306C6D2EABC2649ED05846EDACA84', '易风课堂', 'E:\\phpStudy\\WWW\\blog\\public/face/657c9706041f80f2dc0cf83c7d0c8e9f.jpg', '6', '1');
