/*
Navicat MySQL Data Transfer

Source Server         : 本地
Source Server Version : 50553
Source Host           : localhost:3306
Source Database       : test_blog

Target Server Type    : MYSQL
Target Server Version : 50553
File Encoding         : 65001

Date: 2019-11-07 02:52:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for td_message
-- ----------------------------
DROP TABLE IF EXISTS `td_message`;
CREATE TABLE `td_message` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '0' COMMENT '姓名',
  `phone` varchar(11) NOT NULL DEFAULT '0' COMMENT '手机',
  `content` varchar(200) NOT NULL DEFAULT '0' COMMENT '留言内容',
  `create_time` int(10) NOT NULL DEFAULT '0',
  `update_time` int(10) NOT NULL DEFAULT '0',
  `touid` int(10) NOT NULL DEFAULT '0' COMMENT '留言对象',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='留言表';

-- ----------------------------
-- Records of td_message
-- ----------------------------
INSERT INTO `td_message` VALUES ('2', 'wt', '1112', '<p>222222222</p>', '1571681167', '1571681167', '15');

-- ----------------------------
-- Table structure for yf_article
-- ----------------------------
DROP TABLE IF EXISTS `yf_article`;
CREATE TABLE `yf_article` (
  `aid` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL COMMENT '����',
  `keyword` varchar(100) DEFAULT NULL COMMENT '�ؼ���',
  `desc` varchar(150) DEFAULT NULL COMMENT '����',
  `remark` varchar(150) DEFAULT NULL COMMENT 'ժҪ',
  `content` text COMMENT '����',
  `cid` int(11) NOT NULL COMMENT '����ID',
  `pic` varchar(150) DEFAULT NULL COMMENT 'ͼƬ',
  `source` varchar(45) DEFAULT NULL COMMENT '��Դ',
  `addtime` int(10) DEFAULT NULL COMMENT '����ʱ��',
  `views` int(11) DEFAULT '0' COMMENT '�������',
  `ischeck` tinyint(1) DEFAULT '0' COMMENT '0����˲�ͨ����1�����ͨ��',
  `isnominate` tinyint(1) DEFAULT '0' COMMENT '0�����Ƽ� ��1���Ƽ�',
  `istop` tinyint(1) DEFAULT '0' COMMENT '0�������1���ö�',
  PRIMARY KEY (`aid`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='���±�';

-- ----------------------------
-- Records of yf_article
-- ----------------------------
INSERT INTO `yf_article` VALUES ('1', '博文111', '博文111', '博文111', '博文111', '<p>博文111</p>', '1', 'uploads\\20191012\\5dee45c801fa7734272a43acccb4010b.jpg', '原创', '1570817924', '37', '0', '1', '1');
INSERT INTO `yf_article` VALUES ('2', '博文222', '222', '博文22', '2222', '<p>22222222222</p>', '2', 'uploads\\20191012\\a372cb71f57c1fa6404d000d2122cd4d.jpg', '原创', '1570817940', '0', '0', '1', '1');
INSERT INTO `yf_article` VALUES ('3', '博文5555', '555', '555', '5555', '<p>55555</p>', '5', 'uploads\\20191012\\465a0bbdab9f69fb119b1cf7998086df.jpg', '原创', '1570817960', '2', '0', '1', '1');
INSERT INTO `yf_article` VALUES ('4', '博文44444', '444444', '444', '44444', '<p>4444444444</p>', '4', 'uploads\\20191012\\ba6557ee524252933e44405b25d94f66.jpg', '原创', '1570817985', '1', '0', '1', '1');
INSERT INTO `yf_article` VALUES ('5', '博文66666666666', '6666', '666', '666', '<p>666666</p>', '6', 'uploads\\20191012\\5818c357df41290ce2df51ada9994417.jpg', '原创', '1570818000', '61', '0', '1', '1');
INSERT INTO `yf_article` VALUES ('6', '博文33333333333333', '33333', '333', '333', '<p>33333333</p>', '3', 'uploads\\20191012\\c69a9654159c5ab93cfb9a81f87f8b67.jpg', '原创', '1570818017', '80', '0', '1', '1');

-- ----------------------------
-- Table structure for yf_auth_group
-- ----------------------------
DROP TABLE IF EXISTS `yf_auth_group`;
CREATE TABLE `yf_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) NOT NULL DEFAULT '',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `rules` char(80) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yf_auth_group
-- ----------------------------
INSERT INTO `yf_auth_group` VALUES ('17', '333333', '1', '54,55,56,58,60,59');

-- ----------------------------
-- Table structure for yf_auth_group_access
-- ----------------------------
DROP TABLE IF EXISTS `yf_auth_group_access`;
CREATE TABLE `yf_auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  UNIQUE KEY `uid_group_id` (`uid`,`group_id`),
  KEY `uid` (`uid`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yf_auth_group_access
-- ----------------------------
INSERT INTO `yf_auth_group_access` VALUES ('9', '1');
INSERT INTO `yf_auth_group_access` VALUES ('20', '8');

-- ----------------------------
-- Table structure for yf_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `yf_auth_rule`;
CREATE TABLE `yf_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` char(80) NOT NULL DEFAULT '',
  `title` char(20) NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `condition` char(100) NOT NULL DEFAULT '',
  `pid` int(11) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yf_auth_rule
-- ----------------------------
INSERT INTO `yf_auth_rule` VALUES ('60', '222222', '查件2', '1', '1', '', '58');
INSERT INTO `yf_auth_rule` VALUES ('54', 'app/config', '系统设置', '1', '1', '', '0');
INSERT INTO `yf_auth_rule` VALUES ('55', 'app/config/vip', '会员管理', '1', '1', '', '54');
INSERT INTO `yf_auth_rule` VALUES ('56', 'app/config/vip/user', '普通用户', '1', '1', '', '54');
INSERT INTO `yf_auth_rule` VALUES ('59', 'saas', '插件1', '1', '1', '', '58');
INSERT INTO `yf_auth_rule` VALUES ('58', 'app/config/chajian', '插件管理', '1', '1', '', '54');

-- ----------------------------
-- Table structure for yf_blogcate
-- ----------------------------
DROP TABLE IF EXISTS `yf_blogcate`;
CREATE TABLE `yf_blogcate` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL COMMENT '��������',
  `pic` varchar(150) DEFAULT NULL COMMENT '����ͼƬ',
  `sort` int(11) DEFAULT '100' COMMENT '����',
  `isshow` int(11) DEFAULT '0' COMMENT '�Ƿ���Ŀ��ʾ��0������ʾ,1:��ʾ��',
  `uid` int(11) NOT NULL COMMENT '�����û�',
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='�����';

-- ----------------------------
-- Records of yf_blogcate
-- ----------------------------
INSERT INTO `yf_blogcate` VALUES ('1', 'wy博文分类1', '', '100', '1', '15');
INSERT INTO `yf_blogcate` VALUES ('2', 'wy博文分类2', '', '101', '1', '15');
INSERT INTO `yf_blogcate` VALUES ('3', 'wy博文分类3', '', '102', '1', '15');
INSERT INTO `yf_blogcate` VALUES ('4', 'wy博文分类4', '', '103', '1', '15');
INSERT INTO `yf_blogcate` VALUES ('5', 'wy博文分类5', '', '104', '1', '15');
INSERT INTO `yf_blogcate` VALUES ('6', 'wy博文分类6', '', '105', '1', '15');
INSERT INTO `yf_blogcate` VALUES ('7', 'wy博文分类7', '', '106', '1', '15');

-- ----------------------------
-- Table structure for yf_logs
-- ----------------------------
DROP TABLE IF EXISTS `yf_logs`;
CREATE TABLE `yf_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` char(30) NOT NULL COMMENT '�����ڵ�-�������ͷ���',
  `operator` int(11) NOT NULL DEFAULT '0' COMMENT '����Ա������Աid',
  `description` char(60) NOT NULL COMMENT '����',
  `operate_time` int(10) NOT NULL COMMENT '����ʱ��',
  `operate_ip` varchar(20) NOT NULL COMMENT '����IP',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=141 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yf_logs
-- ----------------------------
INSERT INTO `yf_logs` VALUES ('1', 'Logs/index', '9', '', '1541567597', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('2', 'Logs/index', '9', '', '1541568366', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('3', 'Login/logout', '0', '', '1541568368', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('4', 'Login/index', '0', '', '1541568368', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('5', 'Login/dologin', '9', '', '1541568374', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('6', 'Index/index', '9', '', '1541568378', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('7', 'Index/welcome', '9', '', '1541568378', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('8', 'Logs/index', '9', '', '1541568380', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('9', 'Login/logout', '0', '', '1541568435', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('10', 'Login/index', '0', '', '1541568436', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('11', 'Login/dologin', '20', '', '1541568505', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('12', 'Index/index', '20', '', '1541568508', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('13', 'Index/welcome', '20', '', '1541568509', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('14', 'Logs/index', '20', '', '1541568512', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('15', 'Index/welcome', '20', '', '1541568515', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('16', 'Login/logout', '0', '', '1541568516', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('17', 'Login/index', '0', '', '1541568516', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('18', 'Login/dologin', '0', '', '1541568528', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('19', 'Login/dologin', '0', '', '1541568530', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('20', 'Login/dologin', '0', '', '1541568532', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('21', 'Login/dologin', '9', '', '1541568537', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('22', 'Index/index', '9', '', '1541568540', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('23', 'Index/welcome', '9', '', '1541568541', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('24', 'Logs/index', '9', '', '1541568543', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('25', 'Logs/index', '9', '', '1541568609', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('26', 'Login/logout', '0', '', '1541568641', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('27', 'Login/index', '0', '', '1541568641', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('28', 'Login/dologin', '0', '', '1541568649', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('29', 'Login/dologin', '9', '', '1541568652', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('30', 'Index/index', '9', '', '1541568656', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('31', 'Index/welcome', '9', '', '1541568656', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('32', 'Logs/index', '9', '', '1541568658', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('33', 'Logs/index', '9', '', '1541569805', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('34', 'Logs/index', '9', '', '1541569807', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('35', 'Database/index', '9', '', '1541569809', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('36', 'Manager/index', '9', '', '1541570026', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('37', 'Addons/index', '9', '', '1541570028', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('38', 'Database/index', '9', '', '1541570196', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('39', 'Index/index', '9', '', '1541570198', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('40', 'Index/welcome', '9', '', '1541570198', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('41', 'Database/index', '9', '', '1541570200', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('42', 'Database/backuplst', '9', '', '1541570300', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('43', 'Database/backuplst', '9', '', '1541570362', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('44', 'Database/index', '9', '', '1541571000', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('45', 'Database/backuplst', '9', '', '1541571001', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('46', 'Database/index', '9', '', '1541571447', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('47', 'Database/backuplst', '9', '', '1541571450', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('48', 'Database/index', '9', '', '1541571470', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('49', 'Database/backuplst', '9', '', '1541571471', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('50', 'Database/backuplst', '9', '', '1541571517', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('51', 'Index/index', '9', '', '1541571561', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('52', 'Index/welcome', '9', '', '1541571561', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('53', 'Database/index', '9', '', '1541571564', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('54', 'Database/backuplst', '9', '', '1541571567', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('55', 'Database/backuplst', '9', '', '1541571588', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('56', 'Database/backuplst', '9', '', '1541571621', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('57', 'Database/backuplst', '9', '', '1541571707', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('58', 'Database/index', '9', '', '1541571797', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('59', 'Database/backuplst', '9', '', '1541571806', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('60', 'Database/backuplst', '9', '', '1541571854', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('61', 'Database/download', '9', 'δ֪', '1541571857', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('62', 'Database/download', '9', 'δ֪', '1541571868', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('63', 'Database/download', '9', 'δ֪', '1541572040', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('64', 'Database/backuplst', '9', '', '1541572131', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('65', 'Database/backuplst', '9', '', '1541572206', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('66', 'Database/index', '9', '', '1541572534', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('67', 'Database/backuplst', '9', '', '1541572536', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('68', 'Database/index', '9', '', '1541572572', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('69', 'Database/backuplst', '9', '', '1541572573', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('70', 'Database/backuplst', '9', '', '1541572752', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('71', 'Index/index', '9', '', '1541572764', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('72', 'Index/welcome', '9', '', '1541572765', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('73', 'Database/index', '9', '', '1541572767', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('74', 'Database/backuplst', '9', '', '1541572770', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('75', 'Database/backuplst', '9', '', '1541572838', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('76', 'Database/download', '9', 'δ֪', '1541572840', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('77', 'Database/backuplst', '9', '', '1541572866', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('78', 'Database/download', '9', 'δ֪', '1541572869', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('79', 'Database/index', '9', '', '1541574133', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('80', 'Index/index', '9', '', '1541574134', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('81', 'Index/welcome', '9', '', '1541574135', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('82', 'Database/index', '9', '', '1541574137', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('83', 'Database/backuplst', '9', '', '1541574139', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('84', 'Database/dbbackup', '9', '', '1541574141', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('85', 'Database/backuplst', '9', '', '1541574143', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('86', 'Database/index', '9', '', '1541574156', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('87', 'Database/backuplst', '9', '', '1541574158', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('88', 'Database/del', '9', '', '1541574163', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('89', 'Database/backuplst', '9', '', '1541574165', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('90', 'Database/dbbackup', '9', '', '1541574167', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('91', 'Database/backuplst', '9', '', '1541574168', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('92', 'Database/del', '9', '', '1541574172', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('93', 'Database/backuplst', '9', '', '1541574174', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('94', 'Database/dbbackup', '9', '', '1541574204', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('95', 'Database/backuplst', '9', '', '1541574206', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('96', 'Database/index', '9', '', '1541574288', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('97', 'Database/backuplst', '9', '', '1541574289', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('98', 'Database/dbbackup', '9', '', '1541574293', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('99', 'Database/backuplst', '9', '', '1541574294', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('100', 'Database/index', '9', '', '1541574326', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('101', 'Database/backuplst', '9', '', '1541574327', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('102', 'Database/index', '9', '', '1541574350', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('103', 'Database/backuplst', '9', '', '1541574351', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('104', 'Database/download', '9', 'δ֪', '1541574354', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('105', 'Database/index', '9', '', '1541574618', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('106', 'Index/index', '9', '', '1541575254', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('107', 'Index/welcome', '9', '', '1541575254', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('108', 'Index/index', '9', '', '1541575302', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('109', 'Index/welcome', '9', '', '1541575302', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('110', 'Database/index', '9', '', '1541575404', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('111', 'Database/backuplst', '9', '', '1541575420', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('112', 'Index/index', '9', '', '1541575503', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('113', 'Index/index', '9', '', '1541575504', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('114', 'Index/welcome', '9', '', '1541575505', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('115', 'Index/index', '9', '', '1541575546', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('116', 'Index/welcome', '9', '', '1541575546', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('117', 'Database/index', '9', '', '1541575824', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('118', 'Database/backuplst', '9', '', '1541575827', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('119', 'Database/index', '9', '', '1541576253', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('120', 'Database/backuplst', '9', '', '1541576256', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('121', 'Database/restore', '9', '', '1541576293', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('122', 'Database/backuplst', '9', '', '1541576295', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('123', 'Database/del', '9', '', '1541576296', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('124', 'Database/backuplst', '9', '', '1541576298', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('125', 'Database/index', '9', '', '1541578248', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('126', 'Database/index', '9', '', '1541578249', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('127', 'Database/index', '9', '', '1541578249', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('128', 'Database/index', '9', '', '1541578249', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('129', 'Database/index', '9', '', '1541578249', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('130', 'Database/index', '9', '', '1541578250', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('131', 'Database/index', '9', '', '1541578260', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('132', 'Database/index', '9', '', '1541580783', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('133', 'Database/index', '9', '', '1541580784', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('134', 'Database/index', '9', '', '1541582464', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('135', 'Database/backuplst', '9', '', '1541582464', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('136', 'Database/restore', '9', '', '1541582555', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('137', 'Database/backuplst', '9', '', '1541582557', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('138', 'Database/index', '9', '', '1541582557', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('139', 'Manager/index', '9', '', '1541582559', '127.0.0.1');
INSERT INTO `yf_logs` VALUES ('140', 'Logs/index', '9', '', '1541586171', '127.0.0.1');

-- ----------------------------
-- Table structure for yf_manager
-- ----------------------------
DROP TABLE IF EXISTS `yf_manager`;
CREATE TABLE `yf_manager` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(50) NOT NULL COMMENT '�˺�',
  `passwd` varchar(32) NOT NULL COMMENT '����',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '����Ա״̬',
  `create_time` int(10) NOT NULL COMMENT '����ʱ��',
  `update_time` int(10) NOT NULL COMMENT '����ʱ��',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yf_manager
-- ----------------------------
INSERT INTO `yf_manager` VALUES ('2', 'pengfu', 'pengfuwoaini', '1', '1571162155', '1571162155');
INSERT INTO `yf_manager` VALUES ('3', '11111', '1111111', '1', '1571162398', '1571162398');

-- ----------------------------
-- Table structure for yf_pics
-- ----------------------------
DROP TABLE IF EXISTS `yf_pics`;
CREATE TABLE `yf_pics` (
  `aid` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL COMMENT '����',
  `keyword` varchar(100) DEFAULT NULL COMMENT '�ؼ���',
  `desc` varchar(150) DEFAULT NULL COMMENT '����',
  `remark` varchar(150) DEFAULT NULL COMMENT 'ժҪ',
  `content` text COMMENT '����',
  `cid` int(11) NOT NULL COMMENT '����ID',
  `pic` varchar(150) DEFAULT NULL COMMENT 'ͼƬ',
  `source` varchar(45) DEFAULT NULL COMMENT '��Դ',
  `addtime` int(10) DEFAULT NULL COMMENT '����ʱ��',
  `views` int(11) DEFAULT '0' COMMENT '�������',
  `ischeck` tinyint(1) DEFAULT '0' COMMENT '0����˲�ͨ����1�����ͨ��',
  `isnominate` tinyint(1) DEFAULT '0' COMMENT '0�����Ƽ� ��1���Ƽ�',
  `istop` tinyint(1) DEFAULT '0' COMMENT '0�������1���ö�',
  PRIMARY KEY (`aid`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='���±�';

-- ----------------------------
-- Records of yf_pics
-- ----------------------------
INSERT INTO `yf_pics` VALUES ('1', 'wy图片1', '111111', '11111', '11111111111', '<p>11111</p>', '6', 'uploads\\20191012\\c3af72fa4eb1c5388ca0cb22919fddd9.jpg', '原创', '1570817797', '0', '0', '1', '1');
INSERT INTO `yf_pics` VALUES ('2', 'wy图片1', 'wy图片1', 'wy图片1', 'wy图片1', '<p>wy图片1</p>', '1', 'uploads\\20191012\\8f897b6f3e9dd6df5008b47e498a969e.jpg', '原创', '1570817818', '0', '0', '1', '1');
INSERT INTO `yf_pics` VALUES ('3', 'wy图片5', '5555', '555', '5555', '<p>5555</p>', '5', 'uploads\\20191012\\55758f7156c09acb6b5cac6e7490663d.jpg', '原创', '1570817834', '0', '0', '1', '1');
INSERT INTO `yf_pics` VALUES ('4', 'wy图片6', '6666', '66', '66', '<p>6666</p>', '6', '', '原创', '1570817847', '0', '0', '1', '1');
INSERT INTO `yf_pics` VALUES ('5', 'wy图片333', '333', '333', '3333', '<p>3333</p>', '3', 'uploads\\20191012\\fd628a8f093b4472035fdb81c07fc4e7.jpg', '原创', '1570817866', '0', '0', '1', '1');
INSERT INTO `yf_pics` VALUES ('6', 'wy图片4', '4444', '4444', '4444', '<p>444444444</p>', '4', 'uploads\\20191012\\cfbdfea67183c0fde4dd3d2eb8e77fc0.jpg', '原创', '1570817885', '0', '0', '1', '1');
INSERT INTO `yf_pics` VALUES ('7', 'wy图片222221', '22', '222', '22', '<p>222222</p>', '2', 'uploads\\20191012\\541479adc06b41c959400e668f8c1ca8.jpg', '原创', '1570817902', '0', '0', '1', '1');

-- ----------------------------
-- Table structure for yf_picscate
-- ----------------------------
DROP TABLE IF EXISTS `yf_picscate`;
CREATE TABLE `yf_picscate` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL COMMENT '��������',
  `pic` varchar(150) DEFAULT NULL COMMENT '����ͼƬ',
  `sort` int(11) DEFAULT '100' COMMENT '����',
  `isshow` int(11) DEFAULT '0' COMMENT '�Ƿ���Ŀ��ʾ��0������ʾ,1:��ʾ��',
  `uid` int(11) NOT NULL COMMENT '�����û�',
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='�����';

-- ----------------------------
-- Records of yf_picscate
-- ----------------------------
INSERT INTO `yf_picscate` VALUES ('1', 'wy相册1', '', '100', '1', '15');
INSERT INTO `yf_picscate` VALUES ('2', 'wy相册2', '', '101', '1', '15');
INSERT INTO `yf_picscate` VALUES ('3', 'wy相册3', '', '102', '1', '15');
INSERT INTO `yf_picscate` VALUES ('4', 'wy相册4', '', '103', '1', '15');
INSERT INTO `yf_picscate` VALUES ('5', 'wy相册5', '', '104', '1', '15');
INSERT INTO `yf_picscate` VALUES ('6', 'wy相册6', '', '105', '1', '15');

-- ----------------------------
-- Table structure for yf_user
-- ----------------------------
DROP TABLE IF EXISTS `yf_user`;
CREATE TABLE `yf_user` (
  `uid` int(11) NOT NULL AUTO_INCREMENT COMMENT '�û�id',
  `account` varchar(45) NOT NULL COMMENT '�˺�',
  `passwd` varchar(32) DEFAULT NULL COMMENT '����',
  `email` varchar(45) NOT NULL DEFAULT '' COMMENT '����',
  `regtime` int(10) DEFAULT NULL COMMENT 'ע��ʱ��',
  `salt` varchar(32) DEFAULT NULL COMMENT '��������',
  `token` char(32) DEFAULT NULL COMMENT '������',
  `token_exptime` int(10) DEFAULT '0' COMMENT '��������Ч��',
  `isactive` tinyint(1) DEFAULT '0' COMMENT '����״̬1�����0δ����',
  `logo` varchar(150) DEFAULT NULL COMMENT '����logo',
  `blogtitle` varchar(80) DEFAULT NULL COMMENT '��������',
  `blogname` varchar(150) DEFAULT NULL COMMENT '������',
  `host` char(10) DEFAULT NULL COMMENT '����',
  `info` varchar(200) DEFAULT NULL COMMENT '���ͽ���',
  `content` text COMMENT '��������',
  `face` varchar(150) DEFAULT NULL COMMENT 'ͷ��',
  `nick` varchar(35) DEFAULT NULL COMMENT '�ǳ�',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `account_UNIQUE` (`account`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yf_user
-- ----------------------------
INSERT INTO `yf_user` VALUES ('13', 'pengfu', '10a4afd69699a838e2145519400ac0e5', '291399982@qq.com', '1568707801', '3849918cf5273f3d03ff29819773486e', 'ea95756e04f4093344f27fb7b410c06f', '1568794201', '1', null, null, null, null, null, null, null, null);
INSERT INTO `yf_user` VALUES ('21', 'wywywy', '1ec40363f526b6810dc6034309db5ca6', '719948085@qq.com', '1573055750', 'ffadd96340880dbf21d64e2860dbfa26', '8ed3a4f51b3ab3a2c6fa3edef61ccf00', '1573142150', '1', null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for yf_users
-- ----------------------------
DROP TABLE IF EXISTS `yf_users`;
CREATE TABLE `yf_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userkey` varchar(32) DEFAULT NULL COMMENT '�û�Ωһ��ʶ',
  `nickname` varchar(50) DEFAULT NULL COMMENT '�ǳ�',
  `faceimg` varchar(200) DEFAULT NULL COMMENT '����ͷ��',
  `uid` int(11) DEFAULT NULL COMMENT 'ƽ̨�˺�id',
  `type` tinyint(1) DEFAULT '0' COMMENT 'ƽ̨���ͣ�1��qq��2��΢�ţ�3��΢��',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='�������û�';

-- ----------------------------
-- Records of yf_users
-- ----------------------------
INSERT INTO `yf_users` VALUES ('1', '87E484F084D60C211A49BABDCE564AB6', '', 'E:\\phpStudy\\WWW\\blog\\public/face/2bb47d7a618cf12a6b1e6cabd0c39eb2.jpg', '1', '1');
INSERT INTO `yf_users` VALUES ('3', '085306C6D2EABC2649ED05846EDACA84', '', 'E:\\phpStudy\\WWW\\blog\\public/face/657c9706041f80f2dc0cf83c7d0c8e9f.jpg', '6', '1');
