<?php if (!defined('THINK_PATH')) exit(); /*a:5:{s:76:"D:\phpstudy_pro\WWW\blog\public/../application/index\view\article\index.html";i:1575748834;s:64:"D:\phpstudy_pro\WWW\blog\application\index\view\public\head.html";i:1575748834;s:57:"D:\phpstudy_pro\WWW\blog\application\common\view\top.html";i:1575748834;s:65:"D:\phpstudy_pro\WWW\blog\application\index\view\public\right.html";i:1575748834;s:66:"D:\phpstudy_pro\WWW\blog\application\index\view\public\footer.html";i:1575748834;}*/ ?>
<!doctype html>
<html lang="zh-CN">
<!-- 引入head模板-->
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>王叔叔的博客</title>
    <script src="/static/common/js/layui/layui.js"></script>
    <script src="/static/common/js/layui/layui.all.js"></script>
    <script src="/static/common/js/layer/layer.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="/static/index/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/static/index/css/nprogress.css">
    <link rel="stylesheet" type="text/css" href="/static/index/css/style.css">
    <link rel="stylesheet" type="text/css" href="/static/index/css/font-awesome.min.css">
    <link rel="apple-touch-icon-precomposed" href="/static/index/images/icon/icon.png">
    <link rel="shortcut icon" href="/static/index/images/icon/favicon.ico">
    <script src="/static/index/js/jquery-2.1.4.min.js"></script>

    <!--[if gte IE 9]>
    <script src="/static/index/js/jquery-1.11.1.min.js" type="text/javascript"></script>
    <script src="/static/index/js/html5shiv.min.js" type="text/javascript"></script>
    <script src="/static/index/js/respond.min.js" type="text/javascript"></script>
    <script src="/static/index/js/selectivizr-min.js" type="text/javascript"></script>
    <![endif]-->
    <!--[if lt IE 9]>
    <script>window.location.href='upgrade-browser.html';</script>
    <![endif]-->
</head>
<body class="user-select">
<!--引入头部模板-->
<header class="header">
    <nav class="navbar navbar-default" id="navbar">
        <div class="container">
            <div class="header-topbar hidden-xs link-border">
                <ul class="site-nav topmenu">
                    <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" rel="nofollow">关注本站 <span class="caret"></span></a>
                        <ul class="dropdown-menu header-topbar-dropdown-menu">
                            <li><a data-toggle="modal" data-target="#WeChat" rel="nofollow"><i class="fa fa-weixin"></i> 微信</a></li>
                        </ul>
                    </li>
                </ul>
                <div id="loginstatus">
                    <?php if(\think\Cookie::get('name') == null): ?>
                    <a data-toggle="modal" data-target="#loginModal" class="login" rel="nofollow">Hi,请登录</a>&nbsp;&nbsp;<a href="javascript:;" data-toggle="modal" data-target="#registerModal" class="register" rel="nofollow">我要注册</a>
                    <?php else: ?>
                    欢迎[<?php echo decrypt(\think\Cookie::get('name'),SALT); ?>]&nbsp&nbsp| <a href='<?php echo url("blog/Index/index"); ?>'>个人中心&nbsp | </a><a href="<?php echo url('admin/Index/index'); ?>"> 管理员模式 &nbsp | </a><a href='<?php echo url("index/Login/goout"); ?>'>退出登录</a>
                    <?php endif; ?>
                </div>
            </div>

            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#header-navbar" aria-expanded="false"> <span class="sr-only"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                <h1 class="logo hvr-bounce-in"><a href="" title=""><img src="/static/index/images/logo.png" alt="" style="display:none;">王叔叔的博客</a></h1>
            </div>
            <div class="collapse navbar-collapse" id="header-navbar">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden-index active"><a data-cont="王叔叔博客" href="/">首页</a></li>
                    <li><a href="<?php echo url("index/Article/index"); ?>">博文中心</a></li>
                    <li><a href="about.html">关于我们</a></li>
                </ul>
                <form class="navbar-form visible-xs" action="/Search" method="post">
                    <div class="input-group">
                        <input type="text" name="keyword" class="form-control" placeholder="请输入关键字" maxlength="20" autocomplete="off">
                        <span class="input-group-btn">
            <button class="btn btn-default btn-search" name="search" type="submit">搜索</button>
            </span> </div>
                </form>
            </div>
        </div>
    </nav>
</header>
<script>
    //登录 异步提交  登录成功隐藏注册按钮 并更新状态
    $(function(){
        $("#myloginbtn").on('click',function(){
            var data = $("#myform").serialize();
            $.post("<?php echo url('index/Login/index'); ?>", data,function(res){
                // console.log(res);
                if(res.code==1){
                    layer.msg(res.msg,{time:1000},function(){
                        //bootstrap手动隐藏模态框//
                        $('#loginModal').modal('hide');
                        $("#loginstatus").html("欢迎["+res.name+"]<a href=\"<?php echo url('blog/index/index'); ?>\"> | &nbsp个人中心&nbsp | </a><a href=\"<?php echo url('admin/Index/index'); ?>\"> 管理员模式 &nbsp | </a><a href='<?php echo url('index/Login/goout'); ?>'>退出登录</a>");
                        $("#centre").html("欢迎["+res.name+"]<a href='<?php echo url('index/Login/goout'); ?>'>&nbsp&nbsp退出</a>");
                    });
                }else{
                    layer.msg(res.msg);
                }
            },'json');
            return false
        });

    });
    //注册  异步提交
    $('#btnreg').on('click',function(){
        // 将表单 用serialize方法 将所有 值 序列化
        var form_data=$('#regform').serialize();
        // alert(form_data);
        $.ajax({
            type:'post',
            url:"<?php echo url('index/Login/reg'); ?>",
            data:form_data,//提交的数据
            success:function(res){
                if(res.code=="1"){
                    layer.msg(res.msg,{time:1000},function(){
                        location.href="<?php echo url('index/Index/index'); ?>";
                    });
                }else{
                    layer.msg(res.msg);
                }

            }

        })
    });
</script>
<section class="container">
    <div class="content-wrap">
        <div class="content">
            <div class="title">
                <h3>后端程序</h3>
            </div>
            <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
            <article class="excerpt excerpt-3"><a class="focus" href="<?php echo url("index/article/show",['aid'=>$vo['aid']]); ?>" title="<?php echo $vo['title']; ?>"><img class="thumb" data-original="/<?php echo $vo['pic']; ?>" src="/<?php echo $vo['pic']; ?>" alt="<?php echo $vo['keyword']; ?>"></a>
                <header><a class="cat">后端程序<i></i></a>
                    <h2><a href="<?php echo url("index/article/show",['aid'=>$vo['aid']]); ?>" title="<?php echo $vo['title']; ?>"><?php echo $vo['title']; ?></a></h2>
                </header>
                <p class="meta">
                    <time class="time"><i class="glyphicon glyphicon-time"></i> <?php echo date("Y-m-d H:i:s",$vo['addtime']); ?></time>
                    <span class="views"><i class="glyphicon glyphicon-eye-open"></i> 共<?php echo $vo['views']; ?>人围观</span></p>
                <p class="note"><?php echo $vo['desc']; ?>... </p>
            </article>
            <?php endforeach; endif; else: echo "" ;endif; ?>
            <nav class="pagination">
                <?php echo $list->render(); ?>
            </nav>
        </div>
    </div>
    <!-- 引入right模板-->
    <aside class="sidebar">
    <div class="fixed">
        <div class="widget widget-tabs">
            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#notice" aria-controls="notice" role="tab" data-toggle="tab">网站公告</a></li>
                <li role="presentation"><a href="#centre" aria-controls="centre" role="tab" data-toggle="tab">个人中心</a></li>
                <li role="presentation"><a href="#contact" aria-controls="contact" role="tab" data-toggle="tab">联系站长</a></li>
            </ul>
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane notice active" id="notice">
                    <ul>
                        <li>
                            <time datetime="2016-01-04">01-04</time>
                            <a href="" target="_blank">xxxxxxxx</a></li>
                        <li>
                            <time datetime="2016-01-04">01-04</time>
                            <a target="_blank" href="">XXXXXXXXXXXXXXXXX</a></li>
                        <li>
                            <time datetime="2016-01-04">01-04</time>
                            <a target="_blank" href="">在这个小工具中最多可以调用五条</a></li>
                    </ul>
                </div>
                <div role="tabpanel" class="tab-pane centre" id="centre">
                    <?php if(\think\Cookie::get('name') == null): ?>
                    <h4>需要登录才能进入个人中心</h4>
                    <p> <a data-toggle="modal" data-target="#loginModal" class="btn btn-primary">立即登录</a> <a href="javascript:;" data-toggle="modal" data-target="#registerModal" class="btn btn-default">现在注册</a> </p>
                    <?php else: ?>
                    欢迎[<?php echo decrypt(\think\Cookie::get('name'),SALT); ?>]<a href='<?php echo url('index/Login/goout'); ?>'>&nbsp&nbsp退出登录</a>
                    <?php endif; ?>
                </div>
                <div role="tabpanel" class="tab-pane contact" id="contact">
                    <h2>Email:<br />
                        <a href="mailto:15916477289@163.com" data-toggle="tooltip" data-placement="bottom" title="15916477289@163.com">15916477289@163.com</a></h2>
                </div>
            </div>
        </div>
        <div class="widget widget_search">
            <form class="navbar-form" action="/Search" method="post">
                <div class="input-group">
                    <input type="text" name="keyword" class="form-control" size="35" placeholder="请输入关键字" maxlength="15" autocomplete="off">
                    <span class="input-group-btn">
            <button class="btn btn-default btn-search" name="search" type="submit">搜索</button>
            </span> </div>
            </form>
        </div>
    </div>
    <!--插件扩展 恋爱计时器-->
    <?php echo hook('testhook', ['id'=>1]); ?>
    <div class="widget widget_hot">
        <h3>热门文章</h3>
        <ul>
            <?php if(is_array($hotblogs) || $hotblogs instanceof \think\Collection || $hotblogs instanceof \think\Paginator): $i = 0; $__LIST__ = $hotblogs;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
            <li><a href="<?php echo url('index/Article/show',['aid'=>$vo['aid']]); ?>"><span class="thumbnail"><img class="thumb" data-original="/<?php echo $vo['pic']; ?>" src="/<?php echo $vo['pic']; ?>" alt=""></span><span class="text"><?php echo $vo['remark']; ?></span><span class="muted"><i class="glyphicon glyphicon-time"></i> <?php echo date("Y/m/d H:i:s",$vo['addtime']); ?></span><span class="muted"><i class="glyphicon glyphicon-eye-open"></i><?php echo $vo['views']; ?></span></a></li>
            <?php endforeach; endif; else: echo "" ;endif; ?>

        </ul>
    </div>
</aside>
</section>
<footer class="footer">
    <div class="container">
        <!--插件扩展 友情链接-->
        <h4><?php echo hook('youqinglianjie', ['id'=>1]); ?></h4>
        <p>&copy; 2019 <a href="">blog.com</a> &nbsp; <a href="#" target="_blank" rel="nofollow">#######</a> &nbsp; &nbsp; <a href="#" target="_blank">王叔叔博客</a></p>
    </div>
    <div id="gotop"><a class="gotop"></a></div>
</footer>
<!--微信二维码模态框-->
<div class="modal fade user-select" id="WeChat" tabindex="-1" role="dialog" aria-labelledby="WeChatModalLabel">
    <div class="modal-dialog" role="document" style="margin-top:120px;max-width:280px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="WeChatModalLabel" style="cursor:default;">微信二维码</h4>
            </div>
            <div class="modal-body" style="text-align:center"> <img src="/static/index/images/weixin.jpg" alt="" style="width: 200px;height:200px;cursor:pointer;"/> </div>
        </div>
    </div>
</div>

<!--登录注册模态框-->
<div class="modal fade user-select" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="myform" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="loginModalLabel">登录</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="loginModalUserNmae">用户名</label>
                        <input type="text" class="form-control" id="loginModalUserNmae" placeholder="请输入用户名" autofocus maxlength="15" autocomplete="off" name="account" required>
                    </div>
                    <div class="form-group">
                        <label for="loginModalUserPwd">密码</label>
                        <input type="password" class="form-control" id="loginModalUserPwd" placeholder="请输入密码" maxlength="18" autocomplete="off" name="passwd" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="button" class="btn btn-primary" id="myloginbtn">登录</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--注册模态框-->
<div class="modal fade user-select" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="?" method="post" id="regform">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="registerModalLabel">用户注册</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="account">用户名</label>
                        <input type="text" class="form-control" name="account" id="account" placeholder="请输入用户名" autofocus maxlength="15" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label for="password">密码</label>
                        <input type="password" class="form-control" name="passwd" id="passwd" placeholder="请输入密码" maxlength="18" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label for="repasswd">确认密码</label>
                        <input type="password" class="form-control" name="repasswd" id="passwd"  placeholder="请输入确认密码" maxlength="18" autocomplete="off" required>
                    </div>
                    <div class="form-group">
                        <label for="email">邮箱</label>
                        <input type="email" class="form-control" name="email" id="email"  placeholder="邮箱">
                    </div>
                    <div class="form-group">
                        <label for="code">验证码</label>
                        <input type="text" class="form-control" id="code" name="code" placeholder="请输入验证码" maxlength="18" autocomplete="off" required style="width:105px;display: inline-block;">
                        <img src="<?php echo captcha_src(); ?>" alt="captcha" style="height: 40px;cursor:pointer;" onclick="this.src='<?php echo captcha_src(); ?>?'+Math.random();"/>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="button" id="btnreg" class="btn btn-primary">注册</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="/static/index/js/bootstrap.min.js"></script>
<script src="/static/index/js/jquery.ias.js"></script>
<script src="/static/index/js/scripts.js"></script>

</body>
</html>